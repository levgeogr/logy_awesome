package com.logy.android.presentation.procedures.records

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.logy.android.R
import com.logy.android.databinding.FragmentProceduresRecordsBinding
import com.logy.android.utils.base.BaseFragment
import com.logy.android.utils.base.toolbar.ToolbarConfig
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_ALERT_NON
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_BACK
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_IMAGE
import dagger.hilt.android.AndroidEntryPoint

private const val TOOLBAR_COLOR_CHANGE_VALUE = 5f

@AndroidEntryPoint
class ProceduresRecordsFragment :
    BaseFragment<FragmentProceduresRecordsBinding, ProceduresRecordsVM>(R.layout.fragment_procedures_records) {

    override val viewModel: ProceduresRecordsVM by viewModels()

    override fun createToolbarConfig() = ToolbarConfig(
        toolbar = binding.toolbar,
        homeDrawableRes = ICON_BACK,
        firstIconDrawableRes = ICON_ALERT_NON,
        secondIconDrawableRes = ICON_IMAGE,
        title = getString(R.string.procedures_records_title)
    ).apply {
        setFirstIconTint(R.color.white.getColor())
        setSecondIconTint(R.color.white.getColor())
        setTitleColor(R.color.white.getColor())
        setHomeButtonTint(R.color.white.getColor())
        changeColorWhenRecyclerScroll(
            binding.procedureRecyclerView, R.color.main_white.getColor(), R.color.main_black.getColor(),
            R.color.white.getColor()
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
    }
}
