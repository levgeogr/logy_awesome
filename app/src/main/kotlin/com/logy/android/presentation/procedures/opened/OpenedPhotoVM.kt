package com.logy.android.presentation.procedures.opened

import androidx.lifecycle.MutableLiveData
import com.logy.android.R
import com.logy.android.utils.base.BaseViewModel
import com.logy.android.utils.navigation.DIALOGS
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OpenedPhotoVM @Inject constructor() : BaseViewModel() {
    val photoPath = MutableLiveData("")

    fun sharePhoto() {
        navigateToScreen(DIALOGS.SOCIAL_PHOTO.apply {
            navDirections =
                OpenedPhotoFragmentDirections.sharePhoto(
                    title = R.string.social_photo_dialog_to_share,
                    photo = photoPath.value ?: "",
                    isButtonVisible = false,
                    isDescriptionVisible = false
                )
        })
    }

    fun deletePhoto() {
        navigateToScreen(DIALOGS.DELETE_PHOTO)
    }
}
