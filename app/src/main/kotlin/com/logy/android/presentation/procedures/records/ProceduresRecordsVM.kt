package com.logy.android.presentation.procedures.records

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.TerminalSeparatorType.SOURCE_COMPLETE
import androidx.paging.insertHeaderItem
import androidx.paging.insertSeparators
import androidx.paging.map
import com.logy.android.R
import com.logy.android.domain.models.procedure.ProcedureModel
import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.utils.adapter.ItemVM
import com.logy.android.utils.base.paging.BasePagingViewModel
import com.logy.android.utils.navigation.SCREENS
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProceduresRecordsVM @Inject constructor(
    private val repository: IProcedureRepository,
) : BasePagingViewModel() {

    val loaderVisibilityStatus = MutableLiveData<Boolean>()

    private val proceduresFlow: Flow<PagingData<ItemVM>> = repository.getPagingProcedures()
        .map {
            it.map { model ->
                when (model.status) {
                    ProcedureStatus.PLANNED -> ProcedureItemPlannedVM(model) { navToProcedureInfo(model) }
                    ProcedureStatus.COMPLETE -> ProcedureItemDoneVM(model) { navToProcedureInfo(model) }
                }
            }
                .insertHeaderItem(
                    terminalSeparatorType = SOURCE_COMPLETE,
                    item = ProcedureItemHeaderVM { navToCreateProcedure() }
                )
                .insertSeparators { before: ItemVM?, after: ItemVM? ->
                    when {
                        before is ProcedureItemHeaderVM && after is ProcedureItemPlannedVM -> ProcedureItemTitleVM(R.string.procedures_records_planned)
                        before is ProcedureItemPlannedVM && after is ProcedureItemDoneVM -> ProcedureItemTitleVM(R.string.procedures_records_done)
                        else -> null
                    }
                }
        }

    init {
        viewModelScope.launch {
            proceduresFlow.collect {
                pagingAdapter.submitData(it)
                loaderVisibilityStatus.postValue(false)
            }
        }
    }

    private fun navToProcedureInfo(model: ProcedureModel) {
        navigateToScreen(SCREENS.CREATE_EDIT_PROCEDURE.apply {
            navDirections =
                ProceduresRecordsFragmentDirections.actionNavigationProceduresRecordsToNavigationCreateProcedure(model)
        })
    }

    fun navToCreateProcedure() {
        navigateToScreen(SCREENS.CREATE_EDIT_PROCEDURE)
    }
}
