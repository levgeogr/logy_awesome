package com.logy.android.presentation.procedures.procedure_program

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.SimpleItemAnimator
import com.logy.android.R
import com.logy.android.databinding.FragmentProcedureProgramBinding
import com.logy.android.presentation.daily_care_programs.TOOLBAR_COLOR_CHANGE_VALUE
import com.logy.android.presentation.dialogs.DialogWithConfettiIcon.Bonus
import com.logy.android.presentation.dialogs.DialogWithConfettiIcon.Description
import com.logy.android.presentation.dialogs.DialogWithConfettiIcon.Note
import com.logy.android.presentation.dialogs.DialogWithConfettiIcon.Period
import com.logy.android.presentation.dialogs.DialogWithConfettiIcon.Response
import com.logy.android.utils.base.BaseFragment
import com.logy.android.utils.base.toolbar.ToolbarConfig
import com.logy.android.utils.base.toolbar.ToolbarIconType
import com.logy.android.utils.ui.doOnClick
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProcedureProgramFragment :
    BaseFragment<FragmentProcedureProgramBinding, ProcedureProgramVM>(R.layout.fragment_procedure_program) {

    override val viewModel: ProcedureProgramVM by viewModels()

    override fun createToolbarConfig() = ToolbarConfig(
        toolbar = binding.toolbar,
        homeDrawableRes = ToolbarIconType.ICON_BACK,
        firstIconDrawableRes = ToolbarIconType.ICON_ALERT_NON,
        secondIconDrawableRes = ToolbarIconType.ICON_IMAGE,
    ).apply {
        changeToolbarAndIconsColorWhenScrolling(
            binding.scrollView,
            R.color.main_black.getColor(),
            TOOLBAR_COLOR_CHANGE_VALUE,
            R.color.white.getColor()
        )
        setFirstIconTint(R.color.white.getColor())
        setSecondIconTint(R.color.white.getColor())
        setHomeButtonTint(R.color.white.getColor())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        binding.expandableButtonsRecyclerView.apply {
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }

        with(binding) {
            descriptionButtonTextView.doOnClick {
                viewModel.showDialogWithConfetti(
                    Description,
                    R.string.procedure_program_description,
                    R.string.finish_filling_dialog_description
                )
            }

            bonusButtonTextView.doOnClick {
                viewModel.showDialogWithConfetti(Bonus, R.string.procedure_program_bonus, R.string.finish_filling_dialog_description)
            }

            periodButtonTextView.doOnClick {
                viewModel.showDialogWithConfetti(Period, R.string.procedure_program_period, R.string.finish_filling_dialog_description)
            }

            responseButtonTextView.doOnClick {
                viewModel.showDialogWithConfetti(Response, R.string.procedure_program_response, R.string.finish_filling_dialog_description)
            }

            notationButtonTextView.doOnClick {
                viewModel.showDialogWithConfetti(Note, R.string.procedure_program_notice, R.string.finish_filling_dialog_description)
            }
        }
    }
}
