package com.logy.android.presentation.procedures.records

import androidx.annotation.StringRes
import com.logy.android.R
import com.logy.android.databinding.ItemProcedureRecordsDoneBinding
import com.logy.android.databinding.ItemProcedureRecordsHeaderBinding
import com.logy.android.databinding.ItemProcedureRecordsPlannedBinding
import com.logy.android.databinding.ItemProcedureRecordsTitleBinding
import com.logy.android.domain.models.procedure.ProcedureModel
import com.logy.android.utils.adapter.CommonItemVM
import com.logy.android.utils.helpers.DateFormatHelper.ProcedureCreateDateWithTime

class ProcedureItemTitleVM(
    @StringRes val procedureTitleId: Int
) : CommonItemVM<ItemProcedureRecordsTitleBinding>(R.layout.item_procedure_records_title)

class ProcedureItemDoneVM(
    val procedure: ProcedureModel,
    val navToProcedureInfo: () -> Unit
) : CommonItemVM<ItemProcedureRecordsDoneBinding>(R.layout.item_procedure_records_done) {

    val procedureName = procedure.title
    val procedureDescription = procedure.comment
    val imageBefore = procedure.firstImageBefore
    val imageAfter = procedure.firstImageAfter

    fun onItemClick() {
        navToProcedureInfo()
    }
}

class ProcedureItemPlannedVM(
    val procedure: ProcedureModel,
    val navToProcedureInfo: () -> Unit
) : CommonItemVM<ItemProcedureRecordsPlannedBinding>(R.layout.item_procedure_records_planned) {

    val procedureName = procedure.title
    val procedureTime = ProcedureCreateDateWithTime.format(procedure.plannedTime)
    val showBell = procedure.isNotificated
    val imageBefore = procedure.firstImageBefore

    fun onItemClick() {
        navToProcedureInfo()
    }
}

class ProcedureItemHeaderVM(
    val navToProcedureInfo: () -> Unit
) : CommonItemVM<ItemProcedureRecordsHeaderBinding>(R.layout.item_procedure_records_header) {

    fun onItemClick() {
        navToProcedureInfo()
    }
}
