package com.logy.android.presentation.procedures.info

import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.logy.android.R
import com.logy.android.databinding.FragmentProcedureInfoBinding
import com.logy.android.utils.base.BaseFragment
import com.logy.android.utils.base.toolbar.ToolbarConfig
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_BACK
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_DELETE
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProcedureInfoFragment :
    BaseFragment<FragmentProcedureInfoBinding, ProcedureInfoVM>(R.layout.fragment_procedure_info) {

    override val viewModel: ProcedureInfoVM by viewModels()

    private val getPhotoFromGalleryRequest = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        if (it != null) viewModel.addImage(it.toString())
    }

    override fun createToolbarConfig() = ToolbarConfig(
        toolbar = binding.toolbar,
        homeDrawableRes = ICON_BACK,
        firstIconDrawableRes = ICON_DELETE
    ).apply {
        setFirstIconTint((R.color.pink).getColor())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        viewModel.loadImageFromGallery = ::getPhotoFromGallery
    }

    private fun getPhotoFromGallery() {
        getPhotoFromGalleryRequest.launch(arrayOf("image/jpeg", "image/png"))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getPhotoFromGalleryRequest.unregister()
    }
}
