package com.logy.android.presentation.profile_screens.app_info

import com.logy.android.utils.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AppInfoVM @Inject constructor() : BaseViewModel()
