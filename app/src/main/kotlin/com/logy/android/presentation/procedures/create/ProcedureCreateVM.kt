package com.logy.android.presentation.procedures.create

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.logy.android.data.remote.responses.procedure.ProcedureImage
import com.logy.android.domain.IPrefSettings
import com.logy.android.domain.models.procedure.ProcedureModel
import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.domain.models.procedure.ProcedureStatus.COMPLETE
import com.logy.android.domain.models.procedure.ProcedureStatus.PLANNED
import com.logy.android.domain.usecase.photo.LoadPhotoUseCase
import com.logy.android.domain.usecase.procedure.CreateProcedureUseCase
import com.logy.android.domain.usecase.procedure.DeleteProcedureUseCase
import com.logy.android.domain.usecase.procedure.UpdateProcedureUseCase
import com.logy.android.presentation.procedures.item.ProcedureItemPhoto
import com.logy.android.presentation.procedures.item.ProcedureItemPhotoVM
import com.logy.android.presentation.procedures.notification.NotificationBottomSheet.NotificationTime.ZERO_TIME
import com.logy.android.utils.api.core.onFailure
import com.logy.android.utils.api.core.onSuccess
import com.logy.android.utils.base.BaseViewModel
import com.logy.android.utils.helpers.DateFormatHelper.ProcedureCreateDate
import com.logy.android.utils.helpers.DateFormatHelper.SimpleTime
import com.logy.android.utils.helpers.GetImageFromGalleryHelper
import com.logy.android.utils.navigation.SCREENS.PROCEDURES
import com.logy.android.utils.ui.getTimeFromUnix
import dagger.hilt.android.lifecycle.HiltViewModel
import java.io.File
import java.util.Calendar
import javax.inject.Inject
import kotlinx.coroutines.launch
import timber.log.Timber

private const val TIME_MASK = "%02d:%02d"

@HiltViewModel
class ProcedureCreateVM @Inject constructor(
    private val createProcedureUseCase: CreateProcedureUseCase,
    private val deleteProcedureUseCase: DeleteProcedureUseCase,
    private val updateProcedureUseCase: UpdateProcedureUseCase,
    private val loadPhotoUseCase: LoadPhotoUseCase,
    private val prefs : IPrefSettings
) : BaseViewModel() {

    val procedureName = MutableLiveData("")
    val time = MutableLiveData("")
    val date = MutableLiveData("")
    val notificationTime = MutableLiveData(0L)
    val notificationText = MutableLiveData("")
    val commentMedicationText = MutableLiveData("")
    val notificationEnabled = MutableLiveData(false)
    val procedureStatus = MutableLiveData(PLANNED)
    val isEditProcedure = MutableLiveData(false)
    private val listOfImages = MutableLiveData<List<ProcedureImage>>()
    private val listOfImagesAfterIds = arrayListOf<String>()
    private val listOfImagesBeforeIds = arrayListOf<String>()

    val saveEnabled = SaveEnabledLiveData(
        nameText = procedureName,
        dateText = date,
        timeText = time,
        notificationEnabled = notificationEnabled
    )

    lateinit var loadImageFromGallery: () -> Unit
    lateinit var showNotification: (MutableLiveData<Long>) -> Unit

    val isNotificationEnabled: Boolean
        get() = notificationEnabled.value ?: false

    private var procedureId: String? = null
    private var currentProcedureStatus = PLANNED
    private var unixDateTime = 0L
    private var hours = 0
    private var minutes = 0
    private val notificationCalendar = Calendar.getInstance()

    init {
        initViewModelWithRecycler()
    }

    fun setModel(procedureModel: ProcedureModel?) {
        val model = procedureModel ?: return

        procedureId = model.id
        isEditProcedure.value = true
        procedureName.value = model.title
        commentMedicationText.value = model.comment
        notificationEnabled.value = model.isNotificated
        procedureStatus.value = model.status
        time.value = SimpleTime.format(model.plannedTime)
        date.value = ProcedureCreateDate.format(model.plannedTime)
        val (hours, minutes) = getTimeFromUnix(model.plannedTime)
        this.hours = hours
        this.minutes = minutes
        unixDateTime = model.plannedTime
        listOfImages.value = model.images

        initRecyclerWithImages()

        model.images.forEach {
            if (it.type == "before") {
                listOfImagesBeforeIds.add(it.id.toString())
            } else if (it.type == "after") {
                listOfImagesAfterIds.add(it.id.toString())
            }
        }
    }

    fun setProcedureStatus(status: ProcedureStatus) {
        currentProcedureStatus = status
        procedureStatus.postValue(status)
    }

    private fun initRecyclerWithImages() {
        val listOfImages = listOfImages.value
        val list = adapter?.currentList.let {
            listOfImages?.map { ProcedureItemPhoto(it.url) }?.map { ProcedureItemPhotoVM(it) }
        }
        if (list != null) {
            setData(list)
        }
    }

    fun addImage(uri: String) {
        val item = ProcedureItemPhoto(uri)
        val itemVM = ProcedureItemPhotoVM(item)
        val list = adapter?.currentList.let {
            if (it.isNullOrEmpty()) {
                listOf(itemVM)
            } else {
                it + itemVM
            }
        }
        setData(list)
    }

    fun onImageClick() {
        loadImageFromGallery.invoke()
    }

    fun uploadPhoto(file: File?) {
        viewModelScope.launch {
            if (file != null) {
                loadPhotoUseCase(LoadPhotoUseCase.Params(file, GetImageFromGalleryHelper.PhotoTypes.PROCEDURES))
                    .onSuccess {
                        if (procedureStatus.value == PLANNED) {
                            listOfImagesBeforeIds.add(it.id)
                        } else if (procedureStatus.value == COMPLETE) {
                            listOfImagesAfterIds.add(it.id)
                        }
                    }
                    .onFailure { error ->
                        Timber.d("---- fail -- $error --")
                    }
            }
        }
    }

    fun updateDate(unixTime: Long) {
        unixDateTime = unixTime
        date.value = ProcedureCreateDate.format(unixDateTime)
        notificationCalendar.timeInMillis = unixTime
    }

    fun updateTime(hours: Int, minutes: Int) {
        this.hours = hours
        this.minutes = minutes
        time.value = String.format(TIME_MASK, hours, minutes)
    }

    fun toggleNotificationEnabled() {
        if (notificationTime.value == ZERO_TIME.getTime()) return
        notificationEnabled.value = !(notificationEnabled.value ?: false)
    }

    fun getProcedureDateTime(): Long {
        val calendar = Calendar.getInstance().apply {
            timeInMillis = unixDateTime
            set(Calendar.HOUR_OF_DAY, hours)
            set(Calendar.MINUTE, minutes)
        }
        return calendar.timeInMillis
    }

    fun getNotificationDateTime(): Long {
        return if (notificationTime.value != ZERO_TIME.getTime()) {
            getProcedureDateTime() - notificationTime.value!!
        } else ZERO_TIME.getTime()
    }

    fun saveProcedure(addEventToCalendar: () -> Unit) {
        val isEdit = isEditProcedure.value ?: false
        if (isEdit) {
            updateProcedure()
        } else if (prefs.userSettings.isIntegratedCalendar) {
            createProcedure(addEventToCalendar)
        }
    }

    fun deleteProcedure() {
        val id = procedureId ?: return
        viewModelScope.launch {
            deleteProcedureUseCase(id).onSuccess { navigateBack() }
        }
    }

    fun cancelProcedure() {
        navigateBack()
    }

    fun changeProcedureStatus() {
        if (procedureStatus.value == PLANNED) {
            procedureStatus.value = COMPLETE
            currentProcedureStatus = COMPLETE
        } else {
            procedureStatus.value = PLANNED
            currentProcedureStatus = PLANNED
        }
    }

    private fun createProcedure(addEventToCalendar: () -> Unit) {
        val params = CreateProcedureUseCase.Params(
            title = procedureName.value ?: "",
            dateTime = getProcedureDateTime(),
            comment = commentMedicationText.value ?: "",
            isNotificationNeeded = isNotificationEnabled,
            notificationTime = getNotificationDateTime(),
            procedureStatus = currentProcedureStatus,
            listOfImagesBeforeIds = listOfImagesBeforeIds,
            listOfImagesAfterIds = listOfImagesAfterIds
        )

        viewModelScope.launch {
            createProcedureUseCase(params)
                .onSuccess {
                    addEventToCalendar()
                    navigateToScreen(PROCEDURES)
                }
                .onFailure {
                    Timber.e("Create procedure error: $it")
                }
        }
    }

    private fun updateProcedure() {
        currentProcedureStatus = procedureStatus.value ?: PLANNED
        val id = procedureId ?: return
        val params = UpdateProcedureUseCase.Params(
            id = id,
            title = procedureName.value ?: "",
            dateTime = getProcedureDateTime(),
            comment = commentMedicationText.value ?: "",
            isNotificated = isNotificationEnabled,
            procedureStatus = currentProcedureStatus,
            listOfImagesBeforeIds = listOfImagesBeforeIds,
            listOfImagesAfterIds = listOfImagesAfterIds
        )

        viewModelScope.launch {
            updateProcedureUseCase.invoke(params)
                .onSuccess {
                    navigateToScreen(PROCEDURES.apply {
                        navDirections =
                            CreateProcedureFragmentDirections.actionNavigationCreateProcedureToNavigationProceduresRecords()
                    })
                }
                .onFailure {
                    Timber.e("update procedure error: $it")
                }
        }
    }
}
