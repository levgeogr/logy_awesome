package com.logy.android.presentation.procedures.opened

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.logy.android.R
import com.logy.android.databinding.FragmentOpenedPhotoBinding
import com.logy.android.utils.base.BaseFragment
import com.logy.android.utils.base.toolbar.ToolbarConfig
import com.logy.android.utils.base.toolbar.ToolbarIconType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OpenedPhotoFragment : BaseFragment<FragmentOpenedPhotoBinding, OpenedPhotoVM>(R.layout.fragment_opened_photo) {

    override val viewModel: OpenedPhotoVM by viewModels()
    private val args by navArgs<OpenedPhotoFragmentArgs>()

    override fun createToolbarConfig() = ToolbarConfig(
        binding.toolbar,
        homeDrawableRes = ToolbarIconType.ICON_BACK,
        firstIconDrawableRes = ToolbarIconType.ICON_DELETE,
        secondIconDrawableRes = ToolbarIconType.ICON_SHARE
    ).apply {
        setFirstIconTint((R.color.pink).getColor())
        setSecondIconTint((R.color.pink).getColor())
        setFirstIconListener(viewModel::deletePhoto)
        setSecondIconListener(viewModel::sharePhoto)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        viewModel.photoPath.postValue(args.path)
    }
}
