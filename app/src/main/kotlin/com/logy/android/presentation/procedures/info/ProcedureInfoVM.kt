package com.logy.android.presentation.procedures.info

import androidx.lifecycle.MutableLiveData
import com.logy.android.presentation.procedures.item.ProcedureItemPhoto
import com.logy.android.presentation.procedures.item.ProcedureItemPhotoVM
import com.logy.android.presentation.procedures.item.ProcedureSingleEvent
import com.logy.android.utils.base.BaseViewModel
import com.logy.android.utils.navigation.SCREENS
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProcedureInfoVM @Inject constructor() : BaseViewModel() {

    val userCommentText = MutableLiveData("")
    lateinit var loadImageFromGallery: () -> Unit

    init {
        initViewModelWithRecycler()
    }

    override fun observeEvents(event: Any) {
        when (event) {
            is ProcedureSingleEvent.OpenPhotoEvent -> openPhoto(event.photoPath)
        }
    }

    fun addImage(uri: String) {
        val item = ProcedureItemPhoto(uri)
        val itemVM = ProcedureItemPhotoVM(item)
        val list = adapter?.currentList.let {
            if (it.isNullOrEmpty()) {
                listOf(itemVM)
            } else {
                it + itemVM
            }
        }
        setData(list)
    }

    fun onImageClick() {
        loadImageFromGallery.invoke()
    }

    private fun openPhoto(path: String) {
        navigateToScreen(SCREENS.OPEN_PHOTO.apply {
            navDirections = ProcedureInfoFragmentDirections.sharePhotoPath(path)
        })
    }
}
