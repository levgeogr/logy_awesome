package com.logy.android.presentation.procedures.records.dialog_time_expired

import com.logy.android.utils.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProcedureTimeExpiredDialogVM @Inject constructor() : BaseViewModel()
