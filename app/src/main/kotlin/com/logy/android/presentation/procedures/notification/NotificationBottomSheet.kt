package com.logy.android.presentation.procedures.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.logy.android.R
import com.logy.android.databinding.BottomSheetNotificationsBinding
import com.logy.android.presentation.procedures.create.ProcedureCreateVM
import com.logy.android.utils.ui.doOnClick
import com.logy.android.utils.ui.roundedBackground
import com.logy.android.utils.ui.setColor
import com.logy.android.utils.ui.setDrawableEnd
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationBottomSheet(
    private val procedureCreateVM: ProcedureCreateVM,
    private val time: MutableLiveData<Long>
) : BottomSheetDialogFragment() {

    private val viewModel: NotificationBottomSheetVM by viewModels()
    private val mapOfTime = HashMap<TextView, Long>()
    private val icCircle = R.drawable.ic_circe
    private val icComplete = R.drawable.ic_complete
    private var viewList = emptyList<TextView>()
    private lateinit var binding: BottomSheetNotificationsBinding

    override fun getTheme(): Int = R.style.CustomBottomSheetDialog

    override fun onCreateDialog(savedInstanceState: Bundle?) =
        roundedBackground(super.onCreateDialog(savedInstanceState))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = BottomSheetNotificationsBinding.inflate(inflater, container, false)
        binding.vm = viewModel

        initTimeMap()
        setIcon(mapOfTime.filterValues { it == time.value }.keys.first())
        changeIcon()

        return binding.root
    }

    private fun initTimeMap() {
        with(binding) {
            mapOfTime.apply {
                set(fiveMinutesTextView, FIVE_MIN.getTime())
                set(tenMinutesTextView, TEN_MIN.getTime())
                set(fifteenMinutesTextView, FIFTEEN_MIN.getTime())
                set(thirtyMinutesTextView, THIRTY_MIN.getTime())
                set(hourTextView, HOUR.getTime())
                set(twoHoursTextView, TWO_HOURS.getTime())
                set(threeHoursTextView, THREE_HOURS.getTime())
                set(fourHoursTextView, FOUR_HOURS.getTime())
                set(oneDayTextView, ONE_DAY.getTime())
                set(doNotNotifyTextView, ZERO_TIME.getTime())
            }
        }
    }

    private fun changeIcon() {
        with(binding) {
            viewList = mutableListOf<TextView>().apply {
                add(fiveMinutesTextView)
                add(tenMinutesTextView)
                add(fifteenMinutesTextView)
                add(thirtyMinutesTextView)
                add(hourTextView)
                add(twoHoursTextView)
                add(threeHoursTextView)
                add(fourHoursTextView)
                add(oneDayTextView)
                add(doNotNotifyTextView)
            }
        }
        for (view in viewList) {
            view.doOnClick {
                setIcon(view)
                dismiss()
            }
        }
    }

    private fun setIcon(selectedView: TextView) {
        for (view in viewList) {
            view.setDrawableEnd(icCircle)
            view.setColor(R.color.main_black)
        }
        procedureCreateVM.notificationTime.value = mapOfTime[selectedView]
        procedureCreateVM.notificationText.value = selectedView.text.toString()
        selectedView.setDrawableEnd(icComplete)
        selectedView.setColor(R.color.pink)
    }

    enum class NotificationTime(private val ms: Long) {
        ZERO_TIME(0L),
        FIVE_MIN(300000L),
        TEN_MIN(600000L),
        FIFTEEN_MIN(900000L),
        THIRTY_MIN(1800000L),
        HOUR(3600000L),
        TWO_HOURS(7200000L),
        THREE_HOURS(10800000L),
        FOUR_HOURS(14400000L),
        ONE_DAY(8640000L);

        fun getTime(): Long = ms
    }
}
