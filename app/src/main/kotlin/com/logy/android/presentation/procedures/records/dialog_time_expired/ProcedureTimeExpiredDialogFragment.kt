package com.logy.android.presentation.procedures.records.dialog_time_expired

import android.os.Bundle
import android.text.Spanned
import android.view.View
import androidx.core.text.HtmlCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.logy.android.R
import com.logy.android.databinding.FragmentProcedureTimeExpiredBinding
import com.logy.android.utils.base.BaseDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProcedureTimeExpiredDialogFragment :
    BaseDialogFragment<FragmentProcedureTimeExpiredBinding, ProcedureTimeExpiredDialogVM>(R.layout.dialog_procedure_time_expired) {

    override val viewModel: ProcedureTimeExpiredDialogVM by viewModels()
    private val args by navArgs<ProcedureTimeExpiredDialogFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel

        with(binding) {
            descriptionTextView.text = setText()
            procedureCompletedButton.doOnClick(::dismiss)
            anotherTimeButton.doOnClick(::dismiss)
        }
    }

    private fun setText(): Spanned {
        return HtmlCompat.fromHtml(getString(R.string.procedure_time_expired_description, args.title),
            HtmlCompat.FROM_HTML_MODE_COMPACT)
    }
}
