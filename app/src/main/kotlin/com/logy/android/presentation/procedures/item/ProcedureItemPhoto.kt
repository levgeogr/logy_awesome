package com.logy.android.presentation.procedures.item

data class ProcedureItemPhoto(
    val photo: String
)
