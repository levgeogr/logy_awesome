package com.logy.android.presentation.procedures.item

interface ProcedureSingleEvent {
    data class OpenPhotoEvent(val photoPath: String) : ProcedureSingleEvent
}
