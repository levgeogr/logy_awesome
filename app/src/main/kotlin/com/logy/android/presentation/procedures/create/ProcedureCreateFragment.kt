package com.logy.android.presentation.procedures.create

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.markodevcic.peko.Peko
import com.markodevcic.peko.PermissionResult
import com.logy.android.R
import com.logy.android.databinding.FragmentProcedureCreateBinding
import com.logy.android.domain.IPrefSettings
import com.logy.android.presentation.procedures.notification.NotificationBottomSheet
import com.logy.android.utils.base.BaseFragment
import com.logy.android.utils.base.toolbar.ToolbarConfig
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_ALERT_NON
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_BACK
import com.logy.android.utils.helpers.CalendarHelper
import com.logy.android.utils.helpers.GetImageFromGalleryHelper
import com.logy.android.utils.ui.getDatePicker
import com.logy.android.utils.ui.getTimePicker
import com.logy.android.utils.ui.openAppSettings
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import javax.inject.Inject

private const val DATE_PICKER_TAG = "SELECT_DATE_PICKER"
private const val TIME_PICKER_TAG = "SELECT_TIME_PICKER"
private const val BOTTOM_SHEET_TAG = "NotificationBottomSheetTag"
private const val PROCEDURE_DELETE_TAG = "ProcedureDeleteDialogTag"

@AndroidEntryPoint
class CreateProcedureFragment : BaseFragment<FragmentProcedureCreateBinding, ProcedureCreateVM>(R.layout.fragment_procedure_create) {

    override val viewModel: ProcedureCreateVM by viewModels()

    @Inject
    lateinit var prefs: IPrefSettings

    private val args by navArgs<CreateProcedureFragmentArgs>()

    private val datePicker by lazy {
        getDatePicker(
            title = R.string.create_procedure_select_date,
            selection = args.procedure?.plannedTime ?: System.currentTimeMillis()
        ).apply {
            addOnPositiveButtonClickListener { viewModel.updateDate(it) }
        }
    }

    private val timePicker by lazy {
        getTimePicker(
            title = R.string.cosmo_passport_title,
            selection = args.procedure?.plannedTime ?: System.currentTimeMillis()
        ).apply {
            addOnPositiveButtonClickListener { viewModel.updateTime(hour, minute) }
        }
    }

    private val getPhotoFromGalleryRequest = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        if (it != null) {
            viewModel.addImage(it.toString())
            viewModel.uploadPhoto(getFileFromUri(it))
        }
    }

    override fun createToolbarConfig() = ToolbarConfig(
        toolbar = binding.toolbar,
        homeDrawableRes = ICON_BACK,
        firstIconDrawableRes = ICON_ALERT_NON
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        viewModel.loadImageFromGallery = ::getPhotoFromGallery
        setClickListeners()
        defineNotificationText()
        checkNotificationBell()
        setUpNotificationGroupState()
        with(binding) {

            createProcedureNotificationTextView.doOnClick {
                viewModel.showNotification = ::showBottomSheet.apply { invoke(viewModel.notificationTime) }
            }

            viewModel.setModel(args.procedure)
            args.procedure?.let {
                toolbar.apply {
                    firstIcon = AppCompatResources.getDrawable(requireContext(), ICON_DELETE.drawable)
                    firstIconImageView.setColorFilter(R.color.pink.getColor())
                    firstIconImageView.doOnClick {
                        ProcedureDeleteDialogFragment(viewModel).show(parentFragmentManager, PROCEDURE_DELETE_TAG)
                    }
                }
            }
            commentTextInputEditText.setOnTouchListener { view, motionEvent ->
                view.parent.requestDisallowInterceptTouchEvent(true)
                if ((motionEvent.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                    view.parent.requestDisallowInterceptTouchEvent(false)
                }
                return@setOnTouchListener false
            }
        }
    }

    private fun setUpNotificationGroupState() {
        binding.procedureTypeRadioGroup.setOnCheckedChangeListener { _, checkId ->
            binding.notificationGroup.isVisible = checkId == R.id.plannedRadioButton
        }
    }

    private fun checkNotificationBell() {
        viewModel.notificationText observe {
            if (binding.createProcedureNotificationTextView.text == getString(R.string.bottom_sheet_notification_do_not_notify)) {
                viewModel.notificationEnabled.value = false
            }
        }
    }

    private fun defineNotificationText() {
        viewModel.notificationText observe {
            if (it.isNullOrBlank()) {
                binding.createProcedureNotificationTextView.text = getString(R.string.procedure_info_not_installed)
            } else {
                binding.createProcedureNotificationTextView.text = it
            }
        }
    }

    private fun showBottomSheet(timeInSeconds: MutableLiveData<Long>) =
        NotificationBottomSheet(viewModel, timeInSeconds).show(parentFragmentManager, BOTTOM_SHEET_TAG)

    private fun setClickListeners() {
        binding.dateTextInputField.doOnClick {
            if (!datePicker.isAdded) datePicker.show(parentFragmentManager, DATE_PICKER_TAG)
        }

        binding.timeTextInputEditText.doOnClick {
            if (!timePicker.isAdded) timePicker.show(parentFragmentManager, TIME_PICKER_TAG)
        }

        binding.saveButton.doOnClick {
            viewModel.saveProcedure { addEventToCalendar() }
        }
    }

    private fun getPhotoFromGallery() {
        getPhotoFromGalleryRequest.launch(arrayOf("image/jpeg", "image/png"))
    }

    private fun addEventToCalendar() {
        val calendarHelper = CalendarHelper()

        lifecycleScope.launch {
            val result = Peko.requestPermissionsAsync(
                requireContext(),
                Manifest.permission.WRITE_CALENDAR
            )
            when (result) {
                is PermissionResult.Denied -> openAppSettings()
                is PermissionResult.Granted -> {
                    calendarHelper.addEvent(
                        title = viewModel.procedureName.value ?: "",
                        eventTimeMillis = viewModel.getProcedureDateTime(),
                        additionalRemindTimeMillis = viewModel.getNotificationDateTime(),
                        contentResolver = requireContext().contentResolver
                    )
                }
                else -> Timber.d("Unchecked permission result: $result")
            }
        }
    }

    private fun getFileFromUri(uri: Uri): File? {
        return GetImageFromGalleryHelper
            .PhotoBuilder()
            .buildUriToFile(requireContext(), uri)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getPhotoFromGalleryRequest.unregister()
    }
}
