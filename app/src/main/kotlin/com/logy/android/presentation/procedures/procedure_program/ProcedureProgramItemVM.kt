package com.logy.android.presentation.procedures.procedure_program

import com.logy.android.R
import com.logy.android.databinding.ItemProcedureProgramBinding
import com.logy.android.presentation.procedures.procedure_program.ProcedureProgramVM.ExpandableButtons
import com.logy.android.presentation.procedures.procedure_program.ProcedureProgramVM.SingleEvent
import com.logy.android.utils.adapter.CommonItemVM
import com.logy.android.utils.adapter.ItemVM
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProcedureProgramItemVM(
    val data: ExpandableButtons,
    val isNotSame: Boolean
) : CommonItemVM<ItemProcedureProgramBinding>(R.layout.item_procedure_program) {

    val itemImage = data.image
    val itemName = data.procedureName
    val timeOfDay = data.timeOfDay
    val itemText = data.text
    var itemIsOpen = data.cardViewIsOpen
    private val itemType = data.typeOfButton
    val itemDescription = data.buttonDescription

    fun onArrowClick() {
        CoroutineScope(Dispatchers.IO).launch {
            emitEvent(SingleEvent.OnArrowClickEvent(itemType, itemIsOpen))
        }
    }

    fun onAlertClick() {
        CoroutineScope(Dispatchers.IO).launch {
            emitEvent(SingleEvent.OnAlertClickEvent(itemDescription, itemName))
        }
    }

    override fun areItemsTheSame(oldItem: ItemVM): Boolean {
        return if (isNotSame) false else this.itemType == ((oldItem as? ProcedureProgramItemVM)?.itemType)
    }
}
