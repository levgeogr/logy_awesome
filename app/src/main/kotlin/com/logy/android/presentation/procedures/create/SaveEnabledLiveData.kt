package com.logy.android.presentation.procedures.create

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

typealias NameType = String
typealias DateType = String
typealias TimeType = String
typealias NotificationEnabledType = Boolean

class SaveEnabledLiveData(
    private val nameText: LiveData<NameType>,
    private val dateText: LiveData<DateType>,
    private val timeText: LiveData<TimeType>,
    private val notificationEnabled: LiveData<NotificationEnabledType>
) : MediatorLiveData<Boolean>() {

    override fun onActive() {
        super.onActive()
        addSource(nameText) { checkAndEmit(name = it) }
        addSource(dateText) { checkAndEmit(date = it) }
        addSource(timeText) { checkAndEmit(time = it) }
    }

    override fun onInactive() {
        removeSource(nameText)
        removeSource(dateText)
        removeSource(timeText)
        super.onInactive()
    }

    private fun checkAndEmit(
        name: NameType = nameText.value ?: "",
        date: DateType = dateText.value ?: "",
        time: TimeType = timeText.value ?: "",
    ) {
        val enabled = name.isNotBlank()
            && date.isNotBlank()
            && time.isNotBlank()
        postValue(enabled)
    }
}
