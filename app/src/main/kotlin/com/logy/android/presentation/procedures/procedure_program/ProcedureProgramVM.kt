package com.logy.android.presentation.procedures.procedure_program

import com.logy.android.R
import com.logy.android.presentation.dialogs.DialogWithConfettiIcon
import com.logy.android.presentation.procedures.procedure_program.ProcedureProgramVM.TimesOfDay.EVENING
import com.logy.android.presentation.procedures.procedure_program.ProcedureProgramVM.TimesOfDay.MORNING
import com.logy.android.presentation.procedures.procedure_program.ProcedureProgramVM.TimesOfDay.MORNING_AND_EVENING
import com.logy.android.utils.base.BaseViewModel
import com.logy.android.utils.navigation.DIALOGS
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProcedureProgramVM @Inject constructor() : BaseViewModel() {

    var timeOfDay = EVENING
    private var proceduresProgramList = listOf<ExpandableButtons>()

    init {
        initViewModelWithRecycler()
        getTestData()
    }

    private fun getTestData() {
        val data = listOf(
            ExpandableButtons(
                image = R.drawable.ic_effect_procedure_program,
                procedureName = R.string.procedure_program_makeup_remover,
                text = "Тестовый текст 1",
                timeOfDay = EVENING,
                typeOfButton = ButtonType.MAKEUP_REMOVER,
                buttonDescription = R.string.procedure_program_makeup_remover
            ), ExpandableButtons(
                image = R.drawable.ic_cleansing_procedure_program,
                procedureName = R.string.procedure_program_cleansing,
                text = "Тестовый текст 2",
                timeOfDay = MORNING_AND_EVENING,
                typeOfButton = ButtonType.CLEANSING,
                buttonDescription = R.string.procedure_program_cleansing
            ), ExpandableButtons(
                image = R.drawable.ic_exfoliation_procedure_program,
                procedureName = R.string.procedure_program_exfoliation,
                text = "Тестовый текст 3",
                timeOfDay = EVENING,
                typeOfButton = ButtonType.EXFOLIATION,
                buttonDescription = R.string.procedure_program_exfoliation
            ), ExpandableButtons(
                image = R.drawable.ic_procedure_program_toning,
                procedureName = R.string.procedure_program_toning,
                text = "Тестовый текст 4",
                timeOfDay = MORNING_AND_EVENING,
                typeOfButton = ButtonType.TONING,
                buttonDescription = R.string.procedure_program_toning
            ), ExpandableButtons(
                image = R.drawable.ic_active_care_procedure_program,
                procedureName = R.string.procedure_program_active_care,
                text = "Тестовый текст 5",
                timeOfDay = MORNING_AND_EVENING,
                typeOfButton = ButtonType.ACTIVE_CARE,
                buttonDescription = R.string.procedure_program_active_care
            ), ExpandableButtons(
                image = R.drawable.ic_finishing_cream_procedure_program,
                procedureName = R.string.procedure_program_finishing_cream,
                text = "Тестовый текст 6",
                timeOfDay = MORNING_AND_EVENING,
                typeOfButton = ButtonType.FINISHING_CREAM,
                buttonDescription = R.string.procedure_program_finishing_cream
            ), ExpandableButtons(
                image = R.drawable.ic_procedure_program_uv_protection,
                procedureName = R.string.procedure_program_uv_protection,
                text = "Тестовый текст 7",
                timeOfDay = MORNING,
                typeOfButton = ButtonType.UV_PROTECTION,
                buttonDescription = R.string.procedure_program_uv_protection
            )
        )
        proceduresProgramList = data
        setData(toFilterByTimeOfDay(data, EVENING).map { ProcedureProgramItemVM(data = it, isNotSame = true) })
    }

    fun nightRadioButtonClick() {
        timeOfDay = EVENING
        changeTimeOfDayRadioButton(timeOfDay)
    }

    fun dayRadioButtonClick() {
        timeOfDay = MORNING
        changeTimeOfDayRadioButton(timeOfDay)
    }

    override fun observeEvents(event: Any) {
        when (event) {
            is SingleEvent.OnArrowClickEvent -> {
                val data = proceduresProgramList

                val itemsOnlyEvening = toFilterByTimeOfDay(data, timeOfDay).map { item ->
                    openOrCloseCardView(item, event)
                }

                setData(itemsOnlyEvening.map {
                    ProcedureProgramItemVM(data = it, isNotSame = false)
                })
            }
            is SingleEvent.OnAlertClickEvent ->
                showCareRecommendationsDialog(event.itemName, event.itemDescription)
            }
        }

    private fun showCareRecommendationsDialog(title: Int, text: Int) {
        navigateToScreen(DIALOGS.CARE_RECOMMENDATIONS.apply {
            navDirections =
                ProcedureProgramFragmentDirections.actionNavigationProcedureProgramToCareRecommendationsDialogFragment(
                    dialogTitle = title,
                    dialogText = text
                )
        })
    }

    private fun openOrCloseCardView(item: ExpandableButtons, event: SingleEvent.OnArrowClickEvent) =
        if (item.typeOfButton == event.typeOfButton) {
            item.copy(cardViewIsOpen = !event.itemIsOpenEvent)
        } else {
            item.copy(cardViewIsOpen = false)
        }

    private fun changeTimeOfDayRadioButton(timeOfDay: TimesOfDay) {
        proceduresProgramList = proceduresProgramList.map { item ->
            item.copy(cardViewIsOpen = false)
        }
        val itemsOnlyTimeOfDay = toFilterByTimeOfDay(
            proceduresProgramList,
            timeOfDay
        ).map {
            ProcedureProgramItemVM(
                data = it,
                isNotSame = true
            )
        }
        setData(itemsOnlyTimeOfDay)
    }

    private fun toFilterByTimeOfDay(data: List<ExpandableButtons>, timeOfDay: TimesOfDay?) =
        data.filter { item ->
            item.timeOfDay == timeOfDay || item.timeOfDay == MORNING_AND_EVENING
        }

    sealed interface SingleEvent {
        data class OnArrowClickEvent(val typeOfButton: ButtonType, val itemIsOpenEvent: Boolean) : SingleEvent
        data class OnAlertClickEvent(val itemDescription: Int, val itemName: Int) : SingleEvent
    }

    enum class TimesOfDay {
        MORNING, EVENING, MORNING_AND_EVENING;
    }

    data class ExpandableButtons(
        val image: Int,
        val procedureName: Int,
        val text: String,
        val timeOfDay: TimesOfDay,
        val isDouble: Boolean = timeOfDay == MORNING_AND_EVENING,
        val typeOfButton: ButtonType,
        val cardViewIsOpen: Boolean = false,
        val currentTimeOfDay: TimesOfDay = EVENING,
        val buttonDescription: Int
    )

    enum class ButtonType {
        MAKEUP_REMOVER,
        CLEANSING,
        EXFOLIATION,
        TONING,
        ACTIVE_CARE,
        FINISHING_CREAM,
        UV_PROTECTION
    }

    fun showEffectDialog() {
        navigateToScreen(DIALOGS.EFFECT.apply {
            navDirections =
                ProcedureProgramFragmentDirections.actionNavigationProcedureProgramToEffectDialogFragment(
                    dialogText = R.string.finish_filling_dialog_description
                )
        })
    }

    fun showDialogWithConfetti(icon: DialogWithConfettiIcon, title: Int, text: Int) {
        navigateToScreen(DIALOGS.WITH_CONFETTI.apply {
            navDirections =
                ProcedureProgramFragmentDirections.actionNavigationProcedureProgramToConfettiDialogFragment(
                    dialogIcon = icon,
                    dialogTitle = title,
                    dialogText = text
                )
        })
    }

    fun showDialogVirtualAssistant() {
        navigateToScreen(DIALOGS.VIRTUAL_ASSISTANT.apply {
            navDirections =
                ProcedureProgramFragmentDirections.actionNavigationProcedureProgramToVirtualAssistantDialogFragment(
                    description = R.string.daily_care_programs_assistant_dialog
                )
        })
    }

    fun showPlugDialog() {
        navigateToScreen(DIALOGS.PLUG.apply {
            navDirections =
                ProcedureProgramFragmentDirections.actionNavigationProcedureProgramToPlugDialogFragment(
                    description = R.string.in_progress_suitable_means_in_progress
                )
        })
    }
}
