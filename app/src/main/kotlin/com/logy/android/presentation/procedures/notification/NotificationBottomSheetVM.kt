package com.logy.android.presentation.procedures.notification

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NotificationBottomSheetVM @Inject constructor() : ViewModel()
