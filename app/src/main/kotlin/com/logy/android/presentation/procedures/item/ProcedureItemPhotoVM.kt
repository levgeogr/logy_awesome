package com.logy.android.presentation.procedures.item

import com.logy.android.R
import com.logy.android.databinding.ItemProcedureInfoPhotoBinding
import com.logy.android.utils.adapter.CommonItemVM
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProcedureItemPhotoVM(data: ProcedureItemPhoto) :
    CommonItemVM<ItemProcedureInfoPhotoBinding>(R.layout.item_procedure_info_photo) {

    val photo = data.photo
    fun onImageClick() {
        CoroutineScope(Dispatchers.Main).launch {
            emitEvent(ProcedureSingleEvent.OpenPhotoEvent(photoPath = photo))
        }
    }
}
