package com.logy.android.presentation.profile_screens.app_info

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.logy.android.R
import com.logy.android.databinding.FragmentAppInfoBinding
import com.logy.android.utils.base.BaseFragment
import com.logy.android.utils.base.toolbar.ToolbarConfig
import com.logy.android.utils.base.toolbar.ToolbarIconType.ICON_BACK
import com.logy.android.utils.ui.dp
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AppInfoFragment : BaseFragment<FragmentAppInfoBinding, AppInfoVM>(R.layout.fragment_app_info) {

    override val viewModel: AppInfoVM by viewModels()

    override fun createToolbarConfig() = ToolbarConfig(
        binding.toolbar,
        homeDrawableRes = ICON_BACK,
        title = getString(R.string.app_info_about_app),
        minScrollOffset = 4.dp()
    ).apply {
        observeScroll(binding.scrollView)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
    }
}
