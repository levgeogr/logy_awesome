package com.logy.android.utils.api.core

enum class ErrorCode {
    AuthorizationError,
    RecordNotFoundError,
    AllAttemptsUsedError,
    InternalError,
    ExternalError,
    EmptyResponseError,
    TimeNotPassedError,

    AuthenticationError,
    ValidationError
}
