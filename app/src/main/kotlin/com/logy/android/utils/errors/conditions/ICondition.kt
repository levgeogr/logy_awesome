package com.logy.android.utils.errors.conditions

import com.logy.android.utils.ErrorWrapper

// Условие валидации.
interface ICondition<T> {
    fun validate(data: T): ErrorWrapper
}
