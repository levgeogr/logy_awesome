package com.logy.android.utils.errors.observers

import com.logy.android.utils.ErrorWrapper

// Подписчик на изменение состояние валидатора
interface IObserveValidate {
    fun observe(state: ErrorWrapper)
}
