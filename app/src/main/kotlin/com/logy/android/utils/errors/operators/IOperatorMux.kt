package com.logy.android.utils.errors.operators

import com.logy.android.utils.ErrorWrapper

// Тоже самое, что и IOperator, но с учетом того, что ошибки могут поовторяться по валидаторам
interface IOperatorMux {
    fun getError(validationResult: List<ErrorWrapper>): ErrorWrapper
}
