package com.logy.android.utils.analytics

interface AnalyticMessageHandler {
    fun handleAnalyticMessage(message: AnalyticMessage): AnalyticEvent?
}
