package com.logy.android.utils.base.plug_dialog

import com.logy.android.utils.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlugDialogVM @Inject constructor() : BaseViewModel()
