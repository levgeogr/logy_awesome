package com.logy.android.utils.helpers

import com.logy.android.domain.models.Country.BELARUS
import com.logy.android.domain.models.Country.KAZAKHSTAN
import com.logy.android.domain.models.Country.NO_COUNTRY
import com.logy.android.domain.models.Country.RUSSIA
import com.logy.android.domain.models.Country.UKRAINE
import com.logy.android.domain.models.Country.USA
import java.util.Locale

fun getCountryByLocale() = when (Locale.getDefault().country) {
    findLocaleCountry("KZ") -> KAZAKHSTAN
    findLocaleCountry("RU") -> RUSSIA
    findLocaleCountry("US") -> USA
    findLocaleCountry("UA") -> UKRAINE
    findLocaleCountry("BY") -> BELARUS
    else -> NO_COUNTRY
}

fun findLocaleCountry(country: String) = Locale.getAvailableLocales().find { it.country == country }?.country
