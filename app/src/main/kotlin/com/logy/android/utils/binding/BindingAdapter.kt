package com.logy.android.utils.binding

import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.transition.AutoTransition
import android.transition.Slide
import android.transition.Transition
import android.transition.TransitionManager
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.logy.android.R
import com.logy.android.domain.models.Country
import com.logy.android.domain.models.Country.BELARUS
import com.logy.android.domain.models.Country.KAZAKHSTAN
import com.logy.android.domain.models.Country.RUSSIA
import com.logy.android.domain.models.Country.UKRAINE
import com.logy.android.domain.models.Country.USA
import com.logy.android.utils.ErrorWrapper
import com.logy.android.utils.helpers.DateFormatHelper
import com.logy.android.utils.ui.BindingSpinnerListener
import com.logy.android.utils.ui.Gender.Default
import com.logy.android.utils.ui.Gender.Female
import com.logy.android.utils.ui.Gender.Male
import com.logy.android.utils.ui.dp
import com.logy.android.utils.ui.getOrCreatePhoneMaskWatcher
import ru.tinkoff.decoro.FormattedTextChangeListener
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.FormatWatcher
import java.math.BigDecimal
import java.util.Date

private const val TIME_OF_EXPANDABLE_ANIMATION = 1000L
private const val DELAY_BEFORE_ALPHA_MS = 1000L
private const val DELAY_BEFORE_VISIBILITY_MS = 1500L

@BindingAdapter("app:visibility")
fun setVisibility(v: View, isVisible: Boolean) {
    v.isVisible = isVisible
}

@BindingAdapter("app:invisibility")
fun setInvisibility(v: View, isInvisible: Boolean) {
    v.isInvisible = isInvisible
}

@BindingAdapter("app:date")
fun setDate(view: EditText, date: Date?) {
    val dateString = date?.let { DateFormatHelper.DotsDate.format(it) } ?: ""

    if (!TextUtils.equals(view.text, dateString)) {
        view.setText(dateString)
    }
}

@BindingAdapter(value = ["app:gender", "app:urlImg"])
fun setDrawableGender(view: ImageView, gender: String?, url: String?) {
    if (url.isNullOrBlank()) return
    val placeholderRes = when (gender) {
        Male.gender -> R.drawable.ic_default_profile_male
        else -> R.drawable.ic_default_profile_female
    }
    Glide
        .with(view)
        .load(url)
        .placeholder(placeholderRes)
        .into(view)
}

@BindingAdapter("app:stringUrl")
fun loadImage(view: ImageView, url: String?) {
    if (url.isNullOrBlank()) return
    Glide
        .with(view)
        .load(url)
        .fitCenter()
        .into(view)
}

@BindingAdapter("app:menuSrcCompat")
fun setImage(v: ImageView, image: Drawable) {
    v.setImageDrawable(image)
}

@BindingAdapter("app:drawableId")
fun setImage(view: ImageView, @DrawableRes resId: Int) {
    if (resId == 0) return
    view.setImageResource(resId)
}

@BindingAdapter("app:stringId")
fun setString(view: TextView, @StringRes resId: Int) {
    if (resId == 0) return
    view.text = view.context.getString(resId)
}

@BindingAdapter("app:textColorId")
fun setTextColor(view: TextView, @ColorRes resId: Int) {
    if (resId == 0) return
    view.setTextColor(view.context.getColor(resId))
}

@BindingAdapter(value = ["app:topMargin", "app:BottomMargin", "app:startMargin", "app:endMargin"], requireAll = false)
fun setMargin(v: View, top: Int?, bottom: Int?, start: Int?, end: Int?) {
    v.layoutParams = (v.layoutParams as ViewGroup.MarginLayoutParams).apply {
        topMargin = top?.dp() ?: topMargin
        bottomMargin = bottom?.dp() ?: bottomMargin
        marginStart = start?.dp() ?: marginStart
        marginEnd = end?.dp() ?: marginEnd
    }
}

@BindingAdapter("app:setPrice")
fun setPrice(v: TextView, price: BigDecimal) {
    v.text = "$price ${v.context.getString(R.string.symbol_rub)}"
}

@BindingAdapter("app:recyclerSpaceElement")
fun setRecyclerSpace(recyclerView: RecyclerView, space: Int) {
    val itemDecoration = object : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            parent.adapter?.itemCount?.let {
                if (parent.getChildAdapterPosition(view) != it - 1) outRect.bottom = space.dp()
            }
        }
    }

    recyclerView.addItemDecoration(itemDecoration)
}

@BindingAdapter(value = ["bindErrorWrapperText", "errorDisplayText", "wrapperHintColor"], requireAll = false)
fun bindErrorWrapper(view: View, error: ErrorWrapper, errorDisplayText: Boolean, wrapperHintColor: Int?) {
    if (errorDisplayText) {
        (view as TextView).text = error.getText(view.context)
    } else {
        val (drawable, color) = if (error.isThereError) {
            R.drawable.bg_edit_text_error to R.color.pink
        } else {
            R.drawable.bg_edit_text to R.color.prompt_black
        }

        if (view is Spinner) {
            (view.selectedView as? TextView)?.let {
                view.background = if (error.isThereError) {
                    ContextCompat.getDrawable(view.context, drawable)
                } else {
                    ContextCompat.getDrawable(view.context, drawable)
                }
                it.setTextColor(ContextCompat.getColor(view.context, color))
            }
        } else {
            view.background = ContextCompat.getDrawable(view.context, drawable)
            (view as TextInputEditText).let {
                view.setTextColor(ContextCompat.getColor(view.context, color))
                val hColor = wrapperHintColor ?: ContextCompat.getColor(
                    view.context,
                    if (error.isThereError) color else R.color.normal_gray
                )
                view.setHintTextColor(hColor)
            }
        }
    }
}

@BindingAdapter(value = ["phone", "country"], requireAll = false)
fun setFormattedPhone(view: TextView, rawPhone: String?, country: Country?) {
    val maskWatcher = view.getOrCreatePhoneMaskWatcher(country)
    if ((maskWatcher.mask.hasUserInput() || !rawPhone.isNullOrEmpty()) &&
        maskWatcher.mask.toUnformattedString() != rawPhone
    ) {
        maskWatcher.mask.isShowingEmptySlots
        maskWatcher.maskOriginal.clear()
        maskWatcher.maskOriginal.insertFront(rawPhone)
        maskWatcher.refreshMask()
    }
}

@InverseBindingAdapter(attribute = "phone")
fun getRawPhone(view: TextView): String {
    val maskWatcher = view.getOrCreatePhoneMaskWatcher(null)
    return maskWatcher.mask.toUnformattedString().let { rawPhone ->
        if (rawPhone.startsWith("+")) {
            rawPhone.drop(1)
        } else {
            rawPhone
        }
    }
}

@BindingAdapter(value = ["phoneAttrChanged", "country"], requireAll = false)
fun setPhoneListener(view: TextView, attrChange: InverseBindingListener?, country: Country?) {
    val maskWatcher = view.getOrCreatePhoneMaskWatcher(country)
    maskWatcher.setCallback(object : FormattedTextChangeListener {
        override fun beforeFormatting(oldValue: String?, newValue: String?) = false

        override fun onTextFormatted(formatter: FormatWatcher?, newFormattedText: String?) {
            attrChange?.onChange()
        }
    })
}

@BindingAdapter(value = ["phoneMask", "countryState"], requireAll = false)
fun setMask(view: TextView, mask: String?, countryState: Country?) {
    val maskWatcher = view.getOrCreatePhoneMaskWatcher(countryState)
    mask?.let {
        maskWatcher.swapMask(MaskImpl.createTerminated(UnderscoreDigitSlotsParser().parseSlots(mask)).apply {
            isHideHardcodedHead = true
        })
    }
}

@BindingAdapter("gender")
fun setGender(view: Spinner, newGender: String?) = Unit

@InverseBindingAdapter(attribute = "gender")
fun getGender(view: Spinner): String {
    return when (view.selectedItemPosition) {
        Male.ordinal -> Male.gender
        Female.ordinal -> Female.gender
        else -> Default.gender
    }
}

@BindingAdapter("genderAttrChanged")
fun setGenderListener(view: Spinner, attrChange: InverseBindingListener?) {
    view.onItemSelectedListener?.let { listener ->
        if (listener is BindingSpinnerListener && listener.bindingCallback == null) {
            listener.bindingCallback = { attrChange?.onChange() }
        }
    }
}

@BindingAdapter("unfocusedHint")
fun setHintWhenUnfocused(view: EditText, hint: String?) {
    hint?.let {
        if (!view.hasFocus()) view.hint = hint
        view.setOnFocusChangeListener { _, hasFocus ->
            view.hint = if (hasFocus) null else hint
        }
    }
}

@BindingAdapter("hasMediumWeightInput")
fun setTypefaceBoldForInput(view: EditText, hasMediumWeightInput: Boolean) {
    if (!hasMediumWeightInput) return
    val hintFont = ResourcesCompat.getFont(view.context, R.font.roboto_regular)
    val inputFont = ResourcesCompat.getFont(view.context, R.font.roboto_medium)
    view.addTextChangedListener {
        val typeface = if (it.isNullOrEmpty()) hintFont else inputFont
        view.typeface = typeface
    }
}

@BindingAdapter("app:setUnderlineText")
fun setUnderlineText(view: TextView, isUnderline: Boolean) {
    if (!isUnderline) return
    view.paintFlags = view.paintFlags or Paint.UNDERLINE_TEXT_FLAG
}

@BindingAdapter("animateRotation")
fun setAnimateRotation(view: ImageView, isRotation: Boolean) {
    if (isRotation) {
        val rotate = AnimationUtils.loadAnimation(view.context, R.anim.rotate_180)
        rotate.fillAfter = true
        view.startAnimation(rotate)
    } else {
        val rotate = AnimationUtils.loadAnimation(view.context, R.anim.return_state)
        rotate.fillBefore = true
        view.startAnimation(rotate)
    }
}

@BindingAdapter("expandedCardView")
fun setExpandedCardView(view: View, isExpanded: Boolean) {
    if (view is ConstraintLayout) {
        if (isExpanded) {
            TransitionManager.beginDelayedTransition(view, AutoTransition())
            view.isVisible = true
        } else {
            val transition: Transition = Slide(Gravity.BOTTOM)
            transition.duration = TIME_OF_EXPANDABLE_ANIMATION
            TransitionManager.beginDelayedTransition(view, transition)
            view.isVisible = false
        }
    }
}

@BindingAdapter("app:countryState")
fun setCountry(imageView: ImageView, country: Country) {
    when (country) {
        RUSSIA -> imageView.setImageResource(R.drawable.ic_russia)
        KAZAKHSTAN -> imageView.setImageResource(R.drawable.ic_kazakhstan)
        UKRAINE -> imageView.setImageResource(R.drawable.ic_ukraine)
        USA -> imageView.setImageResource(R.drawable.ic_usa)
        BELARUS -> imageView.setImageResource(R.drawable.ic_belarus)
        else -> imageView.setImageResource(0)
    }
}

@BindingAdapter("app:startAlphaLoaderVisibility")
fun setStartAlphaLoaderVisibility(view: View, visibility: Boolean?) {
    if (visibility == null) return
    view.animate().apply {
        alpha(0F)
        duration = DELAY_BEFORE_ALPHA_MS
    }.start()
    view.postDelayed({
        view.isVisible = visibility
    }, DELAY_BEFORE_VISIBILITY_MS)
}
