package com.logy.android.utils.base.access_limited_dialog

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.logy.android.R
import com.logy.android.databinding.FragmentAccessLimitedDialogBinding
import com.logy.android.utils.base.BaseDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AccessLimitedDialogFragment :
    BaseDialogFragment<FragmentAccessLimitedDialogBinding, AccessLimitedDialogVM>(R.layout.dialog_fragment_access_limited) {

    override val viewModel: AccessLimitedDialogVM by viewModels()
    private val args by navArgs<AccessLimitedDialogFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel

        setDialogData()
    }

    private fun setDialogData() {
        with(binding) {
            counterTextView.text = getString(R.string.access_limited_counter, vm?.referralCodeModel?.value?.referralCounter)
            if (args.isShareAction) {
                descriptionTextView.text = replaceWithPlurals(
                    plural = R.plurals.access_limited_share_plurals,
                    referralCounter = vm?.referralCodeModel?.value?.referralCounter ?: 0,
                    text = R.string.access_limited_condition_to_share,
                )
                inviteCodeTextView.isVisible = false
            } else {
                descriptionTextView.text = replaceWithPlurals(
                    plural = R.plurals.access_limited_invite_plurals,
                    referralCounter = vm?.referralCodeModel?.value?.referralCounter ?: 0,
                    text = R.string.access_limited_condition_to_invite,
                )
                inviteCodeTextView.isVisible = true
                inviteCodeTextView.text = vm?.referralCodeModel?.value?.referralCode
            }
        }
    }

    private fun replaceWithPlurals(plural: Int, referralCounter: Int, text: Int): String {
        return getString(text, requireContext().resources.getQuantityString(plural, referralCounter))
    }
}
