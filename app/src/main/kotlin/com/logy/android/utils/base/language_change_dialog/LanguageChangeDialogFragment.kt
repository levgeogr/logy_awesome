package com.logy.android.utils.base.language_change_dialog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.logy.android.R
import com.logy.android.databinding.FragmentLanguageChangeDialogBinding
import com.logy.android.utils.base.BaseDialogFragment
import com.logy.android.utils.ui.doOnClick
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LanguageChangeDialogFragment :
    BaseDialogFragment<FragmentLanguageChangeDialogBinding, LanguageChangeDialogVM>(R.layout.dialog_fragment_language_change) {

    override val viewModel: LanguageChangeDialogVM by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        binding.button.doOnClick { dismiss() }
    }
}
