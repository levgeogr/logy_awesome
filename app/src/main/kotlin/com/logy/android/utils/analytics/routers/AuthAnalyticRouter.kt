package com.logy.android.utils.analytics.routers

import com.logy.android.utils.analytics.AUTH_BASE_ACTION
import com.logy.android.utils.analytics.AUTH_CATEGORY
import com.logy.android.utils.analytics.AnalyticEvent
import com.logy.android.utils.analytics.AnalyticMessage
import com.logy.android.utils.analytics.AnalyticMessage.AnalyticsLogInEvent
import com.logy.android.utils.analytics.AnalyticMessageHandler

class AuthAnalyticRouter : AnalyticMessageHandler {

    override fun handleAnalyticMessage(message: AnalyticMessage): AnalyticEvent? {
        return when (message) {

            is AnalyticsLogInEvent -> AnalyticEvent(
                AUTH_CATEGORY,
                AUTH_BASE_ACTION,
                "Успешная авторизация пользователя"
            )

            else -> return null
        }
    }
}
