package com.logy.android.utils.base.access_limited_dialog

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.logy.android.domain.models.profile.ReferralCodeModel
import com.logy.android.domain.usecase.personal_information.ReferralCodeUseCase
import com.logy.android.utils.base.BaseViewModel
import com.logy.android.utils.navigation.SCREENS
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch

@HiltViewModel
class AccessLimitedDialogVM @Inject constructor(referralCodeUseCase: ReferralCodeUseCase) : BaseViewModel() {

    val referralCodeModel = MutableLiveData<ReferralCodeModel>()

    init {
        getReferralCode(referralCodeUseCase)
    }

    private fun getReferralCode(referralCodeUseCase: ReferralCodeUseCase) {
        viewModelScope.launch {
            referralCodeUseCase.get(Unit)
                .distinctUntilChanged()
                .collect {
                    referralCodeModel.value = it
                }
        }
    }

    fun navigateToArticle() {
        navigateToScreen(SCREENS.ARTICLE)
    }
}
