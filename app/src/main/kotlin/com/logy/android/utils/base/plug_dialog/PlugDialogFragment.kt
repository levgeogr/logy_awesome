package com.logy.android.utils.base.plug_dialog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.logy.android.R
import com.logy.android.databinding.FragmentPlugDialogBinding
import com.logy.android.utils.base.BaseDialogFragment
import com.logy.android.utils.ui.doOnClick
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlugDialogFragment :
    BaseDialogFragment<FragmentPlugDialogBinding, PlugDialogVM>(R.layout.dialog_fragment_plug) {

    override val viewModel: PlugDialogVM by viewModels()
    private val args by navArgs<PlugDialogFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        binding.button.doOnClick(::dismiss)
        binding.descriptionTextView.text = getString(args.description)
    }
}
