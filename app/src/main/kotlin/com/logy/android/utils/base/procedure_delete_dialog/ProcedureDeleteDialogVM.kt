package com.logy.android.utils.base.procedure_delete_dialog

import com.logy.android.utils.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProcedureDeleteDialogVM @Inject constructor() : BaseViewModel()
