package com.logy.android.utils.helpers

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent

private const val SEND_TYPE_TEXT = "text/plain"
private const val TO_SHARE_TITLE = "ToShare"

class LinksHelper {
    fun openUrlInCustomTab(url: String, context: Context) {
        CustomTabsIntent.Builder().build().apply {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            launchUrl(context, Uri.parse(url))
        }
    }

    fun shareLink(url: String, context: Context) {
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = SEND_TYPE_TEXT
        }
        val shareIntent = Intent.createChooser(sendIntent, TO_SHARE_TITLE)
        context.startActivity(shareIntent)
    }
}
