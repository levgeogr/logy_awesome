package com.logy.android.utils.base.procedure_delete_dialog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.logy.android.R
import com.logy.android.databinding.DialogProcedureDeleteBinding
import com.logy.android.presentation.procedures.create.ProcedureCreateVM
import com.logy.android.utils.base.BaseDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProcedureDeleteDialogFragment(private val procedureCreateVM: ProcedureCreateVM) :
    BaseDialogFragment<DialogProcedureDeleteBinding, ProcedureDeleteDialogVM>(R.layout.dialog_procedure_delete) {

    override val viewModel: ProcedureDeleteDialogVM by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        binding.deleteProcedureButton.doOnClick {
            procedureCreateVM.deleteProcedure()
            dismiss()
        }
    }
}
