package com.logy.android.utils.errors.operators

enum class Operator(val operator: IOperator) {
    Default(DefaultOperator())
}
