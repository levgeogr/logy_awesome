package com.logy.android.utils.analytics

interface AnalyticsPerformer {
    fun sendAnaliticsEvent(event: AnalyticEvent)
}
