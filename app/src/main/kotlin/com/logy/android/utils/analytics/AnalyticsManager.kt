package com.logy.android.utils.analytics

import android.content.Context
import com.logy.android.utils.analytics.firebase.FirebaseAnalyticPerformer
import com.logy.android.utils.analytics.routers.AuthAnalyticRouter

class AnalyticsManager(
    private val context: Context,
    private val firebaseAnalyticPerformer: FirebaseAnalyticPerformer,
    private val authAnalyticRouter: AuthAnalyticRouter,
    private val deviceInfo: DeviceInfo
) {

    private val analytics by lazy {
        listOfNotNull(
            if (isGSMAvailable(context)) firebaseAnalyticPerformer else null
        )
    }

    fun handleAnalyticMessage(message: AnalyticMessage) {
        reportEvent(authAnalyticRouter.handleAnalyticMessage(message))
    }

    private fun reportEvent(event: AnalyticEvent?) {
        if (event == null) return
        event.deviceId = deviceInfo.deviceId
        analytics.forEach { performer ->
            performer.sendAnaliticsEvent(event)
        }
    }

    private fun isGSMAvailable(context: Context) =
        com.google.android.gms.common.GoogleApiAvailabilityLight.getInstance()
            .isGooglePlayServicesAvailable(context) == com.google.android.gms.common.ConnectionResult.SUCCESS
}
