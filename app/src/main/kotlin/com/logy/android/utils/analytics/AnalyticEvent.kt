package com.logy.android.utils.analytics

data class AnalyticEvent(
    val category: String,
    val action: String,
    val label: Any,
    var deviceId: String = ""
)
