package com.logy.android.utils.base.paging

import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.lifecycle.viewModelScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.logy.android.utils.adapter.paging.PagingAdapter
import com.logy.android.utils.base.BaseViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

abstract class BasePagingViewModel : BaseViewModel() {
    val pagingAdapter: PagingAdapter = PagingAdapter()

    init {
        viewModelScope.launch {
            pagingAdapter.observableEvents.receiveAsFlow().collect {
                observeEvents(it)
            }
        }
    }
}

@BindingAdapter("setPagingAdapter")
fun setPagingAdapter(view: RecyclerView, adapter: PagingAdapter) {
    if (view.adapter == null) {
        view.adapter = adapter
        adapter.addLoadStateListener {
            view.isVisible = it.mediator?.refresh is LoadState.NotLoading
        }
    }
}
