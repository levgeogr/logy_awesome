package com.logy.android.utils.ui

import com.logy.android.domain.models.Country
import com.logy.android.utils.helpers.getCountryByLocale

fun changeCountryCode(country: Country?, setCountryMask: (String, String, String) -> Unit) {
    val countryByLocale = country ?: getCountryByLocale()
    setCountryMask.invoke(countryByLocale.countryTag, countryByLocale.countryCode, countryByLocale.rawMask)
}
