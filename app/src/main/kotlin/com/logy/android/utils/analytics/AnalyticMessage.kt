package com.logy.android.utils.analytics

sealed interface AnalyticMessage {
    //Эвенты авторизации
    object AnalyticsLogInEvent : AnalyticMessage
}
