package com.logy.android.utils.errors.observers

import androidx.lifecycle.MutableLiveData
import com.logy.android.utils.ErrorWrapper

class DefaultObserverValidate(private val error: MutableLiveData<ErrorWrapper>) : IObserveValidate {
    override fun observe(state: ErrorWrapper) {
        error.postValue(state)
    }
}
