package com.logy.android.utils.navigation

import androidx.navigation.NavDirections
import com.logy.android.R

enum class SCREENS(
    val screenId: Int,
    val bottomNavigationVisibility: Boolean,
    var navDirections: NavDirections? = null
) {
    //Auth
    SPLASH(R.id.navigation_splash, false),
    ONBOARD(R.id.navigation_onboard, false),
    LOGIN(R.id.navigation_login, false),
    REGISTRATION(R.id.navigation_registration_by_phone, false),
    REGISTRATION_USER_INFO(R.id.navigation_registration_user_info, false),
    RESTORE(R.id.navigation_restore, false),
    RECEIVE_EMAIL(R.id.navigation_receive_email, false),

    //General
    RECEIVE_SMS(R.id.navigation_receive_sms, false),
    NEW_PASSWORD(R.id.navigation_new_pass, false),
    NOTIFICATIONS(R.id.navigation_notifications, true),

    //Main
    MAIN(R.id.navigation_main, true),
    GALLERY_FOR_SOCIAL_NETWORKS(R.id.navigation_gallery_for_social_networks, true),

    //Search
    SEARCH(R.id.navigation_search, true),

    //Favorites
    FAVORITES(R.id.navigation_favorites, true),

    //Profile
    PROFILE(R.id.navigation_profile, true),
    PERSONAL_INFORMATION(R.id.navigation_personal_information, true),
    CHANGE_PASSWORD(R.id.navigation_change_pass, true),
    SETTINGS(R.id.navigation_settings, true),
    APP_INFO(R.id.navigation_app_info, true),
    FEEDBACK(R.id.navigation_feedback, true),
    CHANGE_PHONE(R.id.navigation_change_phone_number, true),
    INVITE_CODE(R.id.navigation_invite_code, true),
    PURCHASE_HISTORY(R.id.navigation_purchase_history, true),
    RECEIVE_SMS_WITH_BOTTOM(R.id.navigation_receive_sms_with_bottom, true),

    //Procedures
    PROCEDURES(R.id.navigation_procedures_records, true),
    PROCEDURE_PROGRAM(R.id.navigation_procedure_program, true),
    PROCEDURE_INFO(R.id.navigation_procedure_info, true),
    DAILY_CARE_PROGRAMS(R.id.navigation_daily_care_programs, true),
    CREATE_EDIT_PROCEDURE(R.id.navigation_create_procedure, true),
    OPEN_PHOTO(R.id.navigation_procedure_open_photo, false),

    //CosmoPassport
    COSMO_PASSPORT(R.id.navigation_cosmo_passport, true),
    COSMO_PASSPORT_REPORT(R.id.navigation_cosmo_passport_report, true),

    //Catalog
    CATALOG(R.id.navigation_catalog, true),
    CARE_RECOMMENDATIONS(R.id.navigation_care_recommendations, true),
    ARTICLES_LIST(R.id.navigation_articles_list, true),
    ARTICLE(R.id.navigation_article, false),
    NEWS_FEED(R.id.navigation_news_feed, true),
    CURRENT_NEWS(R.id.navigation_current_news, true),

    //Comments
    COMMENT_TO_ARTICLE(R.id.navigation_comment_to_article, true),
    COMMENT_TO_HOME_CARE_PROGRAM(R.id.navigation_comment_to_home_care_program, true),

    //CountryCode
    COUNTRY_CODE_LOGGED(R.id.navigation_country_code, true),
    COUNTRY_CODE_NOT_LOGGED(R.id.navigation_country_code, false),
}
