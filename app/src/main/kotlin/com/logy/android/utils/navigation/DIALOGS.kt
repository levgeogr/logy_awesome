package com.logy.android.utils.navigation

import androidx.navigation.NavDirections
import com.logy.android.R

enum class DIALOGS(
    val screenId: Int,
    var navDirections: NavDirections? = null
) {
    //Articles
    ACCESS_LIMITED(R.id.accessLimitedDialogFragment),

    //GalleryForSocial
    SOCIAL_PHOTO(R.id.socialPhotoDialogFragment),

    //Main
    GEOLOCATION(R.id.geoLocationDialogFragment),

    //Диалог заглушка
    PLUG(R.id.plugDialogFragment),

    //Onboard, Registration
    LANGUAGE_CHANGE(R.id.languageChangeDialogFragment),

    //Procedures, CosmoPassport
    VIRTUAL_ASSISTANT(R.id.virtualAssistantDialogFragment),

    //Transformation
    TRANSFORMATION_PLAN(R.id.transformationPlanDialogFragment),

    //ProceduresRecords
    PROCEDURE_TIME_EXPIRED(R.id.procedureTimeExpiredDialog),

    //WithConfetti
    WITH_CONFETTI(R.id.withConfettiDialogFragment),

    //Effect
    EFFECT(R.id.effectDialogFragment),
    FINISH_DIAGNOSTICS(R.id.finishDiagnosticsDialogFragment),

    //CosmoPassportReport
    COSMO_PASSPORT_REPORT_ABOUT_DIALOG(R.id.cosmo_passport_report_about_dialog_fragment),
    COSMO_PASSPORT_REPORT_ALERT_DIALOG(R.id.cosmo_passport_report_alert_dialog_fragment),
    COSMO_PASSPORT_REPORT_INFO_DIALOG(R.id.cosmo_passport_report_info_dialog_fragment),

    //DeletePhoto
    DELETE_PHOTO(R.id.deletePhotoDialog),

    //ArticleTitleDialogFragment
    ARTICLE_TITLES(R.id.articleTitlesDialogFragment),

    //CareRecommendations
    CARE_RECOMMENDATIONS(R.id.careRecommendationsDialogFragment),

    //ProcedureNotification
    PROCEDURE_NOTIFICATION(R.id.notificationBottomSheetDialogFragment)
}
