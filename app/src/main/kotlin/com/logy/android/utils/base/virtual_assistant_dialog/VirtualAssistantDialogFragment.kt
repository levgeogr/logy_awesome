package com.logy.android.utils.base.virtual_assistant_dialog

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.logy.android.R
import com.logy.android.databinding.DialogVirtualAssistantBinding
import com.logy.android.utils.base.BaseDialogFragment
import com.logy.android.utils.ui.doOnClick
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VirtualAssistantDialogFragment : BaseDialogFragment<DialogVirtualAssistantBinding, VirtualAssistantDialogVM>(
    R.layout.dialog_virtual_assistant
) {

    override val viewModel: VirtualAssistantDialogVM by viewModels()
    private val args by navArgs<VirtualAssistantDialogFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(binding) {
            contentTextView.text = getString(args.description)
            understandableMaterialButton.doOnClick(::dismiss)
            contentTextView.movementMethod = ScrollingMovementMethod()
        }
    }
}
