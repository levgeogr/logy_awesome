package com.logy.android.utils.errors.validators

import com.logy.android.utils.ErrorWrapper

// Валидирует источник данных (source) по логике правил (Conditions). Итоговая ошибка вычисляется через operator
interface IValidator {
    fun validation()
    fun getState(): ErrorWrapper
}
