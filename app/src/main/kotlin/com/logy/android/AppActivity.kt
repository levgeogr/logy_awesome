package com.logy.android

import android.graphics.Rect
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import androidx.core.view.forEach
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.logy.android.databinding.ActivityMainBinding
import com.logy.android.utils.CheckInternetConnection
import com.logy.android.utils.navigation.DIALOGS
import com.logy.android.utils.navigation.SCREENS
import com.logy.android.utils.navigation.findNavController
import com.logy.android.utils.navigation.setCurrentDialogScreenWithNavController
import com.logy.android.utils.navigation.setCurrentScreenWithNavController
import com.logy.android.utils.ui.doOnClick
import com.logy.android.utils.ui.showSimpleDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AppActivity : AppCompatActivity() {

    private val viewModel: AppActivityVM by viewModels()

    private lateinit var binding: ActivityMainBinding

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private lateinit var internetConnection: CheckInternetConnection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        firebaseAnalytics = Firebase.analytics
        internetConnection = CheckInternetConnection(binding.internetConnectionStatus)
        setContentView(binding.root)
        activateSecurityMod()
        observeConnectionStatus()
        setBottomNavigationVisibility(false, findNavController(R.id.navHostFragment))
        internetConnection.checkInternetWhenOpenApp()
        binding.addProcedureFab.doOnClick {
            navigateToScreen(SCREENS.CREATE_EDIT_PROCEDURE)
        }
    }

    private fun observeConnectionStatus() {
        viewModel.connectivityLiveData.observe(this, { status ->
            internetConnection.setStatusConnectedResources(status)
        })
    }

    fun navigateToScreen(screen: SCREENS) {
        hideKeyboard()
        findNavController(R.id.navHostFragment).apply {
            setCurrentScreenWithNavController(screen)
            setBottomNavigationVisibility(screen.bottomNavigationVisibility, this)
        }
    }

    fun navigateToScreen(dialog: DIALOGS) {
        findNavController(R.id.navHostFragment).apply {
            setCurrentDialogScreenWithNavController(dialog)
        }
    }

    private fun hideKeyboard() {
        val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun setBottomNavigationVisibility(visibility: Boolean, navController: NavController) {
        if (visibility) {
            binding.bottomNavigationView.setupWithNavController(navController)
        }
        with(binding) {
            bottomBar.isVisible = visibility
            addProcedureFab.isVisible = visibility
        }
        binding.bottomNavigationView.menu.forEach {
            TooltipCompat.setTooltipText(findViewById(it.itemId), null)
        }
    }

    private fun activateSecurityMod() {
        if (BuildConfig.IS_SECURED) {
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        }
    }

    fun selectBottomNavigation(item: MenuItem) {
        if (findNavController(R.id.navHostFragment).currentDestination?.id == item.itemId) return
        when (item.itemId) {
            R.id.navigation_profile -> { navigateToScreen(SCREENS.PROFILE) }
            R.id.navigation_main -> { navigateToScreen(SCREENS.MAIN) }
            R.id.navigation_search -> showDialogWithCurrentDescription(R.string.in_progress_section_description_search)
            R.id.navigation_favorites -> showDialogWithCurrentDescription(R.string.in_progress_section_description_favorites)
        }
    }

    private fun showDialogWithCurrentDescription(@StringRes description: Int) {
        showSimpleDialogFragment(
            R.string.in_progress_section_in_progress,
            description,
            R.string.title_understandable
        )
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN || event.action == MotionEvent.ACTION_UP) {
            val view = currentFocus
            hideKeyboard()
            if (view is EditText) {
                val outRect = Rect()
                view.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    view.clearFocus()
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
}
