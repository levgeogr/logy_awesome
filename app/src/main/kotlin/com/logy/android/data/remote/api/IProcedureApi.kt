package com.logy.android.data.remote.api

import com.logy.android.data.remote.requests.procedure.CreateProcedureRequest
import com.logy.android.data.remote.requests.procedure.UpdateProcedureRequest
import com.logy.android.data.remote.responses.procedure.CreateProcedureBodyResponse
import com.logy.android.data.remote.responses.procedure.ProceduresBodyResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface IProcedureApi {

    @GET("skin_procedures")
    suspend fun getProcedures(@Query("page") page: Int): Response<ProceduresBodyResponse>

    @POST("skin_procedures")
    suspend fun createProcedure(@Body createProcedureRequest: CreateProcedureRequest): Response<CreateProcedureBodyResponse>

    @PUT("skin_procedures/{id}")
    suspend fun updateProcedure(@Path("id") id: String, @Body updateProcedureRequest: UpdateProcedureRequest): Response<CreateProcedureBodyResponse>

    @DELETE("skin_procedures/{id}")
    suspend fun deleteProcedure(@Path("id") id: String): Response<Unit>
}
