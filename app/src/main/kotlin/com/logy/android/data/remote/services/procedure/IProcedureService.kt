package com.logy.android.data.remote.services.procedure

import com.logy.android.data.remote.responses.procedure.ProceduresBodyResponse
import com.logy.android.data.remote.responses.procedure.CreateProcedureBodyResponse
import com.logy.android.utils.api.core.Answer

const val DEFAULT_PROCEDURE_PAGE = 1

interface IProcedureService {
    suspend fun getProcedures(page: Int = DEFAULT_PROCEDURE_PAGE): Answer<ProceduresBodyResponse>

    suspend fun createProcedure(
        title: String,
        dateTime: Long,
        comment: String,
        procedureStatus: String,
        notificationTime: Long,
        isNotificated: Boolean,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<CreateProcedureBodyResponse>

    suspend fun updateProcedure(
        id: String,
        title: String,
        dateTime: Long,
        comment: String,
        procedureStatus: String,
        isNotificated: Boolean,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<CreateProcedureBodyResponse>

    suspend fun deleteProcedure(id: String): Answer<Unit>
}
