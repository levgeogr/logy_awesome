package com.logy.android.data.local.converter

import androidx.room.TypeConverter
import com.logy.android.domain.models.procedure.ProcedureStatus

class ProcedureStatusConverter {

    @TypeConverter
    fun fromProcedureStatus(status: ProcedureStatus): String = status.value

    @TypeConverter
    fun toProcedureStatus(status: String): ProcedureStatus = getStatusFromString(status)

    private fun getStatusFromString(status: String) =
        when (status) {
            ProcedureStatus.COMPLETE.value -> ProcedureStatus.COMPLETE
            else -> ProcedureStatus.PLANNED
        }
}
