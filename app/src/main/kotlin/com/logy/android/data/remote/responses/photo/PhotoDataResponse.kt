package com.logy.android.data.remote.responses.photo

import com.google.gson.annotations.SerializedName
import com.logy.android.domain.models.photo.PhotoModel

data class PhotoDataResponse(
    @SerializedName("data") val data: Data
)

data class Data(
    @SerializedName("attributes") val attributes: Attributes,
    @SerializedName("id") val id: String,
    @SerializedName("type") val type: String
)

data class Attributes(
    @SerializedName("image_file") val imageFile: String
)

fun PhotoDataResponse.toDomain() = PhotoModel(
    id = data.id,
    type = data.type,
    imageFile = data.attributes.imageFile,
)
