package com.logy.android.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.logy.android.data.ProceduresPagingSource
import com.logy.android.data.local.dao.ProceduresDao
import com.logy.android.data.local.entity.ProcedureEntity
import com.logy.android.data.local.entity.toDomain
import com.logy.android.data.remote.responses.procedure.toEntity
import com.logy.android.data.remote.services.procedure.IProcedureService
import com.logy.android.domain.models.procedure.ProcedureModel
import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.utils.api.core.Answer
import com.logy.android.utils.api.core.map
import com.logy.android.utils.api.core.onFailure
import com.logy.android.utils.api.core.onSuccess
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ProcedureRepository(
    private val service: IProcedureService,
    private val proceduresDao: ProceduresDao,
) : IProcedureRepository {

    override fun getProcedures() = proceduresDao.getProcedures()
        .map {
            it.map(ProcedureEntity::toDomain)
        }

    override suspend fun syncProcedures(): Answer<List<ProcedureModel>> {
        var result = Answer<ArrayList<ProcedureModel>>(arrayListOf<ProcedureModel>())

        service.getProcedures()
            .onSuccess {
                proceduresDao.clear()
                proceduresDao.insertProcedure(it.toEntity())
            }
            .onFailure {
                result = Answer(it)
            }

        return result
    }

    override fun getPagingProcedures(): Flow<PagingData<ProcedureModel>> {
        val pager = Pager(
            config = PagingConfig(
                pageSize = 25,
                initialLoadSize = 25,
                prefetchDistance = 3
            ),
            pagingSourceFactory = { ProceduresPagingSource(service) }
        )

        return pager.flow.map { it.map(ProcedureEntity::toDomain) }
    }

    override suspend fun addProcedure(
        title: String,
        dateTime: Long,
        comment: String,
        isNotificated: Boolean,
        notificationTime: Long,
        procedureStatus: ProcedureStatus,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<Unit> {
        return service.createProcedure(
            title,
            dateTime,
            comment,
            procedureStatus.value,
            notificationTime,
            isNotificated,
            listOfImagesBeforeIds,
            listOfImagesAfterIds
        ).map { }
    }

    override suspend fun updateProcedure(
        id: String,
        title: String,
        dateTime: Long,
        comment: String,
        isNotificated: Boolean,
        procedureStatus: ProcedureStatus,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<Unit> =
        service.updateProcedure(
            id = id,
            title = title,
            dateTime = dateTime,
            comment = comment,
            procedureStatus = procedureStatus.value,
            isNotificated = isNotificated,
            listOfImagesBeforeIds = listOfImagesBeforeIds,
            listOfImagesAfterIds = listOfImagesAfterIds
        ).map { }

    override suspend fun deleteProcedure(id: String): Answer<Unit> =
        service.deleteProcedure(id).onSuccess {
            proceduresDao.deleteProcedure(id)
        }
}
