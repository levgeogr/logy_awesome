package com.logy.android.data.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.logy.android.data.remote.responses.procedure.ProcedureImage

class ProcedureImageConverter {
    @TypeConverter
    fun insertImagesInDb(value: List<ProcedureImage>): String {
        val type = object : TypeToken<List<ProcedureImage>>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun getImagesFromDb(value: String): List<ProcedureImage>? {
        return Gson().fromJson(value, object : TypeToken<List<ProcedureImage>>() {}.type)
    }
}
