package com.logy.android.data.remote.requests.procedure

import com.google.gson.annotations.SerializedName

class UpdateProcedureRequest(
    @SerializedName("field_hashes") val fieldHashes: FieldHashes,
)

data class FieldHashes(
    @SerializedName("title") val title: String,
    @SerializedName("planned_time") val plannedTime: String,
    @SerializedName("comment") val comment: String,
    @SerializedName("status") val status: String,
    @SerializedName("is_notificated") val isNotificated: Boolean,
    @SerializedName("images_after_ids") val listOfImagesAfterIds: List<String>?,
    @SerializedName("images_before_ids") val listOfImagesBeforeIds: List<String>?
)
