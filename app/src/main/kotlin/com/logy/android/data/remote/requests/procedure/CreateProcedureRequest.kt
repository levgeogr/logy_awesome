package com.logy.android.data.remote.requests.procedure

import com.google.gson.annotations.SerializedName

class CreateProcedureRequest(
    @SerializedName("title") val title: String,
    @SerializedName("planned_time") val date: String,
    @SerializedName("comment") val comment: String,
    @SerializedName("status") val procedureStatus: String,
    @SerializedName("is_notificated") val isNotificated: Boolean?,
    @SerializedName("notification_datetime") val notificationDatetime: String?,
    @SerializedName("images_after_ids") val listOfImagesAfterIds: List<String>?,
    @SerializedName("images_before_ids") val listOfImagesBeforeIds: List<String>?
)
