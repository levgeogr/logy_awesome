package com.logy.android.data.remote.services.procedure

import com.logy.android.data.remote.api.IProcedureApi
import com.logy.android.data.remote.requests.procedure.CreateProcedureRequest
import com.logy.android.data.remote.requests.procedure.FieldHashes
import com.logy.android.data.remote.requests.procedure.UpdateProcedureRequest
import com.logy.android.data.remote.responses.procedure.CreateProcedureBodyResponse
import com.logy.android.data.remote.responses.procedure.ProceduresBodyResponse
import com.logy.android.utils.api.BaseService
import com.logy.android.utils.api.core.Answer
import com.logy.android.utils.helpers.DateFormatHelper

private const val DEFAULT_PAGE = 1

class ProcedureService(private val api: IProcedureApi) : IProcedureService, BaseService() {

    override suspend fun getProcedures(page: Int): Answer<ProceduresBodyResponse> = apiCall {
        api.getProcedures(page)
    }

    override suspend fun createProcedure(
        title: String,
        dateTime: Long,
        comment: String,
        procedureStatus: String,
        notificationTime: Long,
        isNotificated: Boolean,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<CreateProcedureBodyResponse> = apiCall {
        val slashedDate = DateFormatHelper.CreateProcedureTimeStamp.format(dateTime)
        val slashedNotificationDate = DateFormatHelper.CreateProcedureTimeStamp.format(notificationTime)
        api.createProcedure(
            CreateProcedureRequest(
                title = title,
                date = slashedDate,
                comment = comment,
                procedureStatus = procedureStatus,
                isNotificated = isNotificated,
                notificationDatetime = slashedNotificationDate,
                listOfImagesBeforeIds = listOfImagesBeforeIds,
                listOfImagesAfterIds = listOfImagesAfterIds
            )
        )
    }

    override suspend fun updateProcedure(
        id: String,
        title: String,
        dateTime: Long,
        comment: String,
        procedureStatus: String,
        isNotificated: Boolean,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ) = apiCall {
        val formattedDate = DateFormatHelper.CreateProcedureTimeStamp.format(dateTime)
        val fieldHashes = FieldHashes(
            title = title,
            plannedTime = formattedDate,
            comment = comment,
            status = procedureStatus,
            isNotificated = isNotificated,
            listOfImagesBeforeIds = listOfImagesBeforeIds,
            listOfImagesAfterIds = listOfImagesAfterIds
        )
        api.updateProcedure(id, UpdateProcedureRequest(fieldHashes))
    }

    override suspend fun deleteProcedure(id: String) = apiCall {
        api.deleteProcedure(id)
    }
}
