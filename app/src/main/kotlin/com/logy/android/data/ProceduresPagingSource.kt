package com.logy.android.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.logy.android.data.local.entity.ProcedureEntity
import com.logy.android.data.remote.responses.procedure.ProceduresBodyResponse
import com.logy.android.data.remote.responses.procedure.toEntity
import com.logy.android.data.remote.services.procedure.IProcedureService

private const val DEFAULT_PAGE = 1

class ProceduresPagingSource(
    val service: IProcedureService
) : PagingSource<Int, ProcedureEntity>() {

    override fun getRefreshKey(state: PagingState<Int, ProcedureEntity>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProcedureEntity> {
        val page = params.key ?: DEFAULT_PAGE
        val response =
            (service.getProcedures(page).value as ProceduresBodyResponse).data
        return LoadResult.Page<Int, ProcedureEntity>(
            data = response.map { it.toEntity() },
            prevKey = if (page == DEFAULT_PAGE) null else page - 1,
            nextKey = if (response.isEmpty()) null else page + 1
        )
    }
}
