package com.logy.android.data.remote.interceptors

import com.logy.android.domain.IPrefSettings
import okhttp3.Interceptor
import okhttp3.Response

class AuthTokenInterceptor(private val preferences: IPrefSettings) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val token = preferences.token
        val originalRequest = chain.request()

        val newRequest = if (token.isNullOrEmpty()) {
            originalRequest.newBuilder().build()
        } else {
            originalRequest.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", token)
                .build()
        }

        val response = chain.proceed(newRequest)
        val tokenFromHeader = response.headers["authorization"]
        if (!tokenFromHeader.isNullOrEmpty()) {
            preferences.token = tokenFromHeader
        }

        return response
    }
}
