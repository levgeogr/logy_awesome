package com.logy.android.data.local.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.logy.android.data.local.entity.ProcedureEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ProceduresDao {

    @Query("SELECT * FROM procedure_table")
    fun getProcedures(): Flow<List<ProcedureEntity>>

    @Query("SELECT * FROM procedure_table")
    fun getProceduresPaging(): PagingSource<Int, ProcedureEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProcedure(entity: ProcedureEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProcedure(entities: List<ProcedureEntity>)

    @Query("DELETE FROM procedure_table WHERE id = :id")
    suspend fun deleteProcedure(id: String)

    @Query("DELETE FROM procedure_table")
    suspend fun clear()
}
