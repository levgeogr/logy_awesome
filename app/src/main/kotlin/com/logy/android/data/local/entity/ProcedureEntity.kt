package com.logy.android.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.logy.android.data.local.converter.ProcedureImageConverter
import com.logy.android.data.local.converter.ProcedureStatusConverter
import com.logy.android.data.remote.responses.procedure.ProcedureImage
import com.logy.android.domain.models.procedure.ProcedureModel
import com.logy.android.domain.models.procedure.ProcedureStatus

@Entity(tableName = "procedure_table")
class ProcedureEntity(
    @PrimaryKey val id: String,
    val type: String,
    val title: String,
    val plannedTime: Long,
    val comment: String,
    val isNotificated: Boolean,
    @TypeConverters(ProcedureStatusConverter::class) val status: ProcedureStatus,
    @TypeConverters(ProcedureImageConverter::class) val images: List<ProcedureImage>
)

fun ProcedureEntity.toDomain() = ProcedureModel(
    id,
    type,
    title,
    plannedTime,
    comment,
    status,
    isNotificated,
    images = images,
    firstImageBefore = images.find { it.type == "before" },
    firstImageAfter = images.find { it.type == "after" }
)
