package com.logy.android.data.remote.api

import com.logy.android.data.remote.requests.personal_information.ChangeNumberRequest
import com.logy.android.data.remote.requests.personal_information.ConfirmNewPhoneNumberRequest
import com.logy.android.data.remote.responses.personal_information.PersonalInformationResponse
import com.logy.android.data.remote.requests.personal_information.EditProfileRequest
import com.logy.android.data.remote.requests.personal_information.FeedBackRequest
import com.logy.android.data.remote.requests.profile.ChangePasswordAuthorizedRequest
import com.logy.android.data.remote.responses.personal_information.ChangePhoneNumberResponse
import com.logy.android.data.remote.responses.personal_information.ReferralCodeResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface IPersonalInformationApi {

    @GET("profile")
    suspend fun getPersonalInformation(): Response<PersonalInformationResponse>

    @POST("profile_edit")
    suspend fun editProfileInformation(
        @Body editProfileRequest: EditProfileRequest
    ): Response<PersonalInformationResponse>

    @POST("feedbacks")
    suspend fun sendFeedBack(
        @Body feedBackRequest: FeedBackRequest
    ): Response<Unit>

    @GET("profile/referal_code")
    suspend fun getReferralCode(): Response<ReferralCodeResponse>

    @POST("change_phone_number")
    suspend fun changePhoneNumber(
        @Body changeNumberRequest: ChangeNumberRequest
    ): Response<ChangePhoneNumberResponse>

    @POST("confirmation_new_phone_number")
    suspend fun confirmNewPhoneNumber(
        @Body confirmNewPhoneNumberRequest: ConfirmNewPhoneNumberRequest
    ): Response<Unit>

    @POST("change_password_authorized")
    suspend fun changePasswordAuthorized(
        @Body changePasswordAuthorizedRequest: ChangePasswordAuthorizedRequest
    ): Response<Unit>
}
