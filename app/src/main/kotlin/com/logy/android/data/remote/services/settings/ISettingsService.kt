package com.logy.android.data.remote.services.settings

import com.logy.android.data.remote.responses.settings.SettingsResponse
import com.logy.android.utils.api.core.Answer

interface ISettingsService {

    suspend fun getSettings(): Answer<SettingsResponse>

    suspend fun editSettings(
        isReceivingNews: Boolean,
        isReceivingNotifications: Boolean,
        isIntegratedCalendar: Boolean
    ): Answer<SettingsResponse>

    suspend fun deleteProfile(): Answer<Unit>
}
