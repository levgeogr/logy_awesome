package com.logy.android.data.remote.services.personal_information

import com.logy.android.data.remote.responses.personal_information.ChangePhoneNumberResponse
import com.logy.android.data.remote.responses.personal_information.PersonalInformationResponse
import com.logy.android.data.remote.responses.personal_information.ReferralCodeResponse
import com.logy.android.utils.api.core.Answer

interface IPersonalInformationService {
    suspend fun getPersonalInformation(): Answer<PersonalInformationResponse>
    suspend fun editProfileInformation(
        avatarId: String?,
        name: String?,
        email: String?,
        birthday: String?,
        gender: String?
    ): Answer<PersonalInformationResponse>

    suspend fun sendFeedBack(message: String): Answer<Unit>
    suspend fun getReferralCode(): Answer<ReferralCodeResponse>
    suspend fun changePhoneNumber(
        password: String,
        phoneNumber: Long
    ): Answer<ChangePhoneNumberResponse>

    suspend fun confirmNewPhoneNumber(
        phoneNumber: Long,
        confirmationCode: Int
    ): Answer<Unit>

    suspend fun changePasswordAuthorized(
        oldPassword: String,
        newPassword: String,
        newPasswordConfirmation: String
    ): Answer<Unit>
}
