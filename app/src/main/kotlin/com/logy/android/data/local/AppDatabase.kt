package com.logy.android.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.logy.android.data.local.converter.ProcedureImageConverter
import com.logy.android.data.local.converter.ProcedureStatusConverter
import com.logy.android.data.local.dao.ProceduresDao
import com.logy.android.data.local.entity.ProcedureEntity

@Database(
    entities = [ProcedureEntity::class],
    version = 3,
    exportSchema = false
)
@TypeConverters(ProcedureStatusConverter::class, ProcedureImageConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun procedureDao(): ProceduresDao
}
