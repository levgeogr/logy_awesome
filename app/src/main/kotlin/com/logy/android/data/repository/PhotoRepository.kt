package com.logy.android.data.repository

import com.logy.android.data.remote.responses.photo.PhotoDataResponse
import com.logy.android.data.remote.responses.photo.toDomain
import com.logy.android.data.remote.services.photo.IPhotoService
import com.logy.android.domain.models.photo.PhotoModel
import com.logy.android.domain.reopsitory.IPhotoRepository
import com.logy.android.utils.api.core.Answer
import com.logy.android.utils.api.core.map
import com.logy.android.utils.helpers.GetImageFromGalleryHelper
import com.logy.android.utils.helpers.GetImageFromGalleryHelper.PhotoTypes
import java.io.File
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class PhotoRepository(private val photoService: IPhotoService) : IPhotoRepository {

    override suspend fun sendPhoto(photo: File, type: PhotoTypes): Answer<PhotoModel> {
        return photoService.sendPhoto(toMultipart(photo), stringToRequestBody(type.type)).map(PhotoDataResponse::toDomain)
    }

    private fun toMultipart(file: File): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            "file",
            file.name,
            getRequestBodyFromFile(file, "image")
        )
    }

    private fun getRequestBodyFromFile(file: File, contentType: String): RequestBody {
        return GetImageFromGalleryHelper
            .PhotoBuilder()
            .buildFileToRequestBody(file, contentType)
    }

    private fun stringToRequestBody(type: String): RequestBody {
        return type.toRequestBody("text/plain".toMediaTypeOrNull())
    }
}
