package com.logy.android.data.remote.services.personal_information

import com.logy.android.data.remote.api.IPersonalInformationApi
import com.logy.android.data.remote.requests.personal_information.ChangeNumberRequest
import com.logy.android.data.remote.requests.personal_information.ConfirmNewPhoneNumberRequest
import com.logy.android.data.remote.requests.personal_information.EditProfileRequest
import com.logy.android.data.remote.requests.personal_information.FeedBackRequest
import com.logy.android.data.remote.requests.profile.ChangePasswordAuthorizedRequest
import com.logy.android.data.remote.responses.personal_information.ChangePhoneNumberResponse
import com.logy.android.data.remote.responses.personal_information.PersonalInformationResponse
import com.logy.android.data.remote.responses.personal_information.ReferralCodeResponse
import com.logy.android.utils.api.BaseService
import com.logy.android.utils.api.core.Answer

class PersonalInformationService(
    private val api: IPersonalInformationApi
) : IPersonalInformationService, BaseService() {

    override suspend fun getPersonalInformation(): Answer<PersonalInformationResponse> = apiCall {
        api.getPersonalInformation()
    }

    override suspend fun editProfileInformation(
        avatarId: String?,
        name: String?,
        email: String?,
        birthday: String?,
        gender: String?
    ): Answer<PersonalInformationResponse> = apiCall {
        api.editProfileInformation(EditProfileRequest(avatarId, name, email, birthday, gender))
    }

    override suspend fun changePhoneNumber(password: String, phoneNumber: Long): Answer<ChangePhoneNumberResponse> = apiCall {
        api.changePhoneNumber(ChangeNumberRequest(password, phoneNumber))
    }

    override suspend fun confirmNewPhoneNumber(
        phoneNumber: Long,
        confirmationCode: Int
    ): Answer<Unit> = apiCall {
        api.confirmNewPhoneNumber(ConfirmNewPhoneNumberRequest(phoneNumber, confirmationCode))
    }

    override suspend fun sendFeedBack(message: String): Answer<Unit> = apiCall {
        api.sendFeedBack(FeedBackRequest(message))
    }

    override suspend fun getReferralCode(): Answer<ReferralCodeResponse> = apiCall {
        api.getReferralCode()
    }

    override suspend fun changePasswordAuthorized(
        oldPassword: String,
        newPassword: String,
        newPasswordConfirmation: String
    ) = apiCall {
        api.changePasswordAuthorized(
            ChangePasswordAuthorizedRequest(
                oldPassword,
                newPassword,
                newPasswordConfirmation
            )
        )
    }
}
