package com.logy.android.data.remote.services.photo

import com.logy.android.data.remote.responses.photo.PhotoDataResponse
import com.logy.android.utils.api.core.Answer
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface IPhotoService {
    suspend fun sendPhoto(photo: MultipartBody.Part, type: RequestBody): Answer<PhotoDataResponse>
}
