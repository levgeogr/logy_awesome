package com.logy.android.data.repository

import android.content.Context
import androidx.core.content.edit
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.logy.android.domain.models.Country
import com.logy.android.domain.models.SettingsModel
import com.logy.android.domain.models.profile.PersonalInformationModel
import com.logy.android.domain.models.profile.ReferralCodeModel
import com.logy.android.domain.IPrefSettings
import com.logy.android.utils.helpers.fromJson
import com.logy.android.utils.helpers.getCountryByName
import com.logy.android.utils.helpers.toJson

private const val PREFS = "logy_prefs"
private const val AUTH_DATA_KEY = "auth_data"
private const val IS_ONBOARD_KEY = "is_onboard"
private const val USER_SETTINGS_KEY = "user_settings"
private const val USER_KEY = "user_key"
private const val COUNTRY_CODE_KEY = "country_code"
private const val REFERRAL_CODE_KEY = "referral_code"

class PrefSettings(context: Context) : IPrefSettings {

    private val settings = EncryptedSharedPreferences.create(
        context,
        PREFS,
        MasterKey.Builder(context).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build(),
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    override var token: String?
        get() = settings.getString(AUTH_DATA_KEY, null)
        set(value) = settings.edit().putString(AUTH_DATA_KEY, value).apply()

    override var isOnboardNotPassed: Boolean
        get() = settings.getBoolean(IS_ONBOARD_KEY, true)
        set(value) = settings.edit().putBoolean(IS_ONBOARD_KEY, value).apply()

    override var userSettings: SettingsModel
        get() = settings.getString(USER_SETTINGS_KEY, null).fromJson() ?: SettingsModel(
            isReceivingNews = true,
            isReceivingNotifications = true,
            isIntegratedCalendar = true
        )
        set(value) = settings.edit().putString(USER_SETTINGS_KEY, value.toJson()).apply()

    override var user: PersonalInformationModel?
        get() = settings.getString(USER_KEY, null).fromJson()
        set(value) = settings.edit().putString(USER_KEY, value.toJson()).apply()

    override var currentCountry: Country?
        get() = getCountryByName(settings.getString(COUNTRY_CODE_KEY, null))
        set(value) = settings.edit().putString(COUNTRY_CODE_KEY, value?.countryTag).apply()

    override var referralCode: ReferralCodeModel
        get() = settings.getString(REFERRAL_CODE_KEY, null).fromJson() ?: ReferralCodeModel(
            referralCode = "",
            referralCounter = 0
        )
        set(value) = settings.edit().putString(REFERRAL_CODE_KEY, value.toJson()).apply()

    @Synchronized
    override suspend fun clear() {
        settings.edit {
            remove(AUTH_DATA_KEY)
            remove(IS_ONBOARD_KEY)
            remove(USER_SETTINGS_KEY)
            remove(USER_KEY)
            remove(REFERRAL_CODE_KEY)
        }
    }
}
