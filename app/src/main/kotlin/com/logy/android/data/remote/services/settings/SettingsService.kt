package com.logy.android.data.remote.services.settings

import com.logy.android.data.remote.api.ISettingsApi
import com.logy.android.data.remote.requests.settings.EditSettingsRequest
import com.logy.android.data.remote.responses.settings.SettingsResponse
import com.logy.android.utils.api.BaseService
import com.logy.android.utils.api.core.Answer

class SettingsService(val api: ISettingsApi) : ISettingsService, BaseService() {

    override suspend fun getSettings(): Answer<SettingsResponse> = apiCall {
        api.getSettings()
    }

    override suspend fun editSettings(
        isReceivingNews: Boolean,
        isReceivingNotifications: Boolean,
        isIntegratedCalendar: Boolean
    ): Answer<SettingsResponse> = apiCall {
        api.editSettings(
            EditSettingsRequest(
                isReceivingNews,
                isReceivingNotifications,
                isIntegratedCalendar
            )
        )
    }

    override suspend fun deleteProfile(): Answer<Unit> = apiCall {
        api.deleteProfile()
    }
}
