package com.logy.android.data.remote.responses.procedure

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.logy.android.data.local.entity.ProcedureEntity
import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.utils.helpers.DateFormatHelper
import kotlinx.parcelize.Parcelize

private const val MILLIS_IN_SEC = 1000

class ProceduresBodyResponse(
    @SerializedName("data") val data: List<ProcedureResponse>,
)

class CreateProcedureBodyResponse(
    @SerializedName("data") val data: ProcedureResponse,
)

data class ProcedureResponse(
    @SerializedName("id") val id: String,
    @SerializedName("type") val type: String,
    @SerializedName("attributes") val attributes: ProcedureAttributes,
)

data class ProcedureAttributes(
    @SerializedName("title") val title: String,
    @SerializedName("planned_time") val plannedTime: String,
    @SerializedName("comment") val comment: String?,
    @SerializedName("status") val status: String,
    @SerializedName("is_notificated") val isNotificated: Boolean,
    @SerializedName("images") val images: List<ProcedureImage>
)

@Parcelize
data class ProcedureImage(
    @SerializedName("id") val id: Int,
    @SerializedName("type") val type: String,
    @SerializedName("url") val url: String
) : Parcelable

fun ProceduresBodyResponse.toEntity() = this.data.map(ProcedureResponse::toEntity)

fun ProcedureResponse.toEntity() = ProcedureEntity(
    id = id,
    type = type,
    title = attributes.title,
    plannedTime = DateFormatHelper.FullDateTimeStamp.parse(attributes.plannedTime)?.time ?: 0,
    comment = attributes.comment ?: "",
    status = getStatusFromString(attributes.status),
    isNotificated = attributes.isNotificated,
    images = attributes.images
)

private fun getStatusFromString(status: String) =
    when (status) {
        ProcedureStatus.COMPLETE.value -> ProcedureStatus.COMPLETE
        else -> ProcedureStatus.PLANNED
    }
