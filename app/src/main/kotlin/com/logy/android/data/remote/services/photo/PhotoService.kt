package com.logy.android.data.remote.services.photo

import com.logy.android.data.remote.api.IPhotoApi
import com.logy.android.data.remote.responses.photo.PhotoDataResponse
import com.logy.android.utils.api.BaseService
import com.logy.android.utils.api.core.Answer
import okhttp3.MultipartBody
import okhttp3.RequestBody

class PhotoService(private val api: IPhotoApi) : IPhotoService, BaseService() {

    override suspend fun sendPhoto(photo: MultipartBody.Part, type: RequestBody): Answer<PhotoDataResponse> = apiCall {
        api.sendPhoto(photo, type)
    }
}
