package com.logy.android.di.usecases

import com.logy.android.domain.reopsitory.IPersonalInformationRepository
import com.logy.android.domain.usecase.settings.DeleteProfileUseCase
import com.logy.android.domain.IPrefSettings
import com.logy.android.domain.reopsitory.ISettingsRepository
import com.logy.android.domain.usecase.personal_information.ChangeNumberUseCase
import com.logy.android.domain.usecase.personal_information.ConfirmNewPhoneNumberUseCase
import com.logy.android.domain.usecase.personal_information.EditPersonalInformationUseCase
import com.logy.android.domain.usecase.personal_information.GetPersonalInformationUseCase
import com.logy.android.domain.usecase.personal_information.SendFeedBackUseCase
import com.logy.android.domain.usecase.profile.ChangePasswordAuthorizedUseCase
import com.logy.android.domain.usecase.personal_information.ReferralCodeUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object PersonalInformationUseCaseModule {

    @Provides
    fun providePersonalInformationUseCase(personalInformationRepository: IPersonalInformationRepository) =
        GetPersonalInformationUseCase(personalInformationRepository)

    @Provides
    fun provideEditProfileUseCase(personalInformationRepository: IPersonalInformationRepository) =
        EditPersonalInformationUseCase(personalInformationRepository)

    @Provides
    fun provideChangeNumberUseCase(personalInformationRepository: IPersonalInformationRepository) =
        ChangeNumberUseCase(personalInformationRepository)

    @Provides
    fun provideDeleteProfileUseCase(settingsRepository: ISettingsRepository) =
        DeleteProfileUseCase(settingsRepository)

    @Provides
    fun provideConfirmNewPhoneNumberUseCase(personalInformationRepository: IPersonalInformationRepository) =
        ConfirmNewPhoneNumberUseCase(personalInformationRepository)

    @Provides
    fun provideSendFeedBackUseCase(personalInformationRepository: IPersonalInformationRepository) =
        SendFeedBackUseCase(personalInformationRepository)

    @Provides
    fun provideChangePasswordAuthorizedUseCase(personalInformationRepository: IPersonalInformationRepository) =
        ChangePasswordAuthorizedUseCase(personalInformationRepository)

    @Provides
    fun provideReferralCodeUseCase(personalInformationRepository: IPersonalInformationRepository, prefSettings: IPrefSettings) =
        ReferralCodeUseCase(personalInformationRepository, prefSettings)
}
