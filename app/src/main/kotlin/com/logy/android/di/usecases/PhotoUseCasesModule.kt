package com.logy.android.di.usecases

import com.logy.android.domain.reopsitory.IPhotoRepository
import com.logy.android.domain.usecase.photo.LoadPhotoUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object PhotoUseCasesModule {

    @Provides
    fun providePhotoUseCase(iPhotoRepository: IPhotoRepository) = LoadPhotoUseCase(iPhotoRepository)
}
