package com.logy.android.di.usecases

import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.domain.usecase.procedure.CreateProcedureUseCase
import com.logy.android.domain.usecase.procedure.DeleteProcedureUseCase
import com.logy.android.domain.usecase.procedure.UpdateProcedureUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object ProcedureUseCaseModule {

    @Provides
    fun provideCreateProcedureUseCase(procedureRepository: IProcedureRepository) = CreateProcedureUseCase(procedureRepository)

    @Provides
    fun provideDeleteProceduresUseCase(procedureRepository: IProcedureRepository) = DeleteProcedureUseCase(procedureRepository)

    @Provides
    fun provideUpdateProceduresUseCase(procedureRepository: IProcedureRepository) = UpdateProcedureUseCase(procedureRepository)
}
