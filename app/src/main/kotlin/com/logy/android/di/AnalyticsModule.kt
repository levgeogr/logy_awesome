package com.logy.android.di

import android.content.Context
import com.logy.android.utils.analytics.AnalyticsManager
import com.logy.android.utils.analytics.DeviceInfo
import com.logy.android.utils.analytics.firebase.FirebaseAnalyticPerformer
import com.logy.android.utils.analytics.routers.AuthAnalyticRouter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AnalyticsModule {

    @Provides
    @Singleton
    fun provideFirebaseAnalyticPerformer(@ApplicationContext context: Context) = FirebaseAnalyticPerformer(context)

    @Provides
    @Singleton
    fun provideAuthAnalyticRouter() = AuthAnalyticRouter()

    @Provides
    @Singleton
    fun provideDeviceInfo(@ApplicationContext context: Context) = DeviceInfo(context)

    @Provides
    @Singleton
    fun provideAnalyticsManager(
        @ApplicationContext context: Context,
        firebaseAnalyticPerformer: FirebaseAnalyticPerformer,
        authAnalyticRouter: AuthAnalyticRouter,
        deviceInfo: DeviceInfo
    ) = AnalyticsManager(context, firebaseAnalyticPerformer, authAnalyticRouter, deviceInfo)
}
