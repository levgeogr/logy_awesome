package com.logy.android.di

import android.content.Context
import com.logy.android.data.local.dao.ProceduresDao
import com.logy.android.data.remote.services.auth.IAuthService
import com.logy.android.data.remote.services.personal_information.IPersonalInformationService
import com.logy.android.data.remote.services.photo.IPhotoService
import com.logy.android.data.remote.services.procedure.IProcedureService
import com.logy.android.data.remote.services.settings.ISettingsService
import com.logy.android.data.repository.AuthRepository
import com.logy.android.data.repository.PersonalInformationRepository
import com.logy.android.data.repository.PhotoRepository
import com.logy.android.data.repository.PrefSettings
import com.logy.android.data.repository.ProcedureRepository
import com.logy.android.data.repository.SettingsRepository
import com.logy.android.domain.reopsitory.IAuthRepository
import com.logy.android.domain.reopsitory.IPersonalInformationRepository
import com.logy.android.domain.reopsitory.IPhotoRepository
import com.logy.android.domain.IPrefSettings
import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.domain.reopsitory.ISettingsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun providePreferencesRepository(@ApplicationContext context: Context): IPrefSettings = PrefSettings(context)

    @Provides
    fun provideAuthRepository(service: IAuthService): IAuthRepository = AuthRepository(service)

    @Provides
    @Singleton
    fun providePersonalInformationRepository(
        service: IPersonalInformationService,
        prefRepository: IPrefSettings
    ): IPersonalInformationRepository = PersonalInformationRepository(service, prefRepository)

    @Provides
    fun provideProcedureRepository(service: IProcedureService, proceduresDao: ProceduresDao): IProcedureRepository =
        ProcedureRepository(service, proceduresDao)

    @Provides
    @Singleton
    fun providePhotoRepository(service: IPhotoService): IPhotoRepository = PhotoRepository(service)

    @Provides
    fun provideSettingsRepository(service: ISettingsService): ISettingsRepository = SettingsRepository(service)
}
