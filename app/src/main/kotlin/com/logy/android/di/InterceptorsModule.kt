package com.logy.android.di

import com.logy.android.data.remote.interceptors.AuthTokenInterceptor
import com.logy.android.domain.IPrefSettings
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import okhttp3.logging.HttpLoggingInterceptor

@Module
@InstallIn(SingletonComponent::class)
object InterceptorsModule {

    @Provides
    @Singleton
    fun provideAuthTokenInterceptor(prefSettings: IPrefSettings) =
        AuthTokenInterceptor(prefSettings)

    @Provides
    @Singleton
    fun provideLoggingInterceptor() = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
}
