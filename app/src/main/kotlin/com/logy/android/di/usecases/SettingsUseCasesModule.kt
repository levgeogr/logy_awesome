package com.logy.android.di.usecases

import com.logy.android.domain.reopsitory.ISettingsRepository
import com.logy.android.domain.usecase.settings.EditSettingsUseCase
import com.logy.android.domain.usecase.settings.GetSettingsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object SettingsUseCasesModule {

    @Provides
    fun provideGetSettingsUseCase(settingsRepository: ISettingsRepository) = GetSettingsUseCase(settingsRepository)

    @Provides
    fun provideEditSettingsUseCase(settingsRepository: ISettingsRepository) = EditSettingsUseCase(settingsRepository)
}
