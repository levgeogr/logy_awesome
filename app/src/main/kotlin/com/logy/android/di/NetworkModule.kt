package com.logy.android.di

import com.logy.android.data.remote.api.IAuthApi
import com.logy.android.data.remote.api.IPersonalInformationApi
import com.logy.android.data.remote.api.IPhotoApi
import com.logy.android.data.remote.api.IProcedureApi
import com.logy.android.data.remote.api.ISettingsApi
import com.logy.android.data.remote.interceptors.AuthTokenInterceptor
import com.logy.android.data.remote.services.auth.AuthService
import com.logy.android.data.remote.services.auth.IAuthService
import com.logy.android.data.remote.services.personal_information.IPersonalInformationService
import com.logy.android.data.remote.services.personal_information.PersonalInformationService
import com.logy.android.data.remote.services.photo.IPhotoService
import com.logy.android.data.remote.services.photo.PhotoService
import com.logy.android.data.remote.services.procedure.IProcedureService
import com.logy.android.data.remote.services.procedure.ProcedureService
import com.logy.android.data.remote.services.settings.ISettingsService
import com.logy.android.data.remote.services.settings.SettingsService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val TIMEOUT = 20L

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val HTTP_SCHEME = "https://"
    private const val API_PATH = "logy.joy-dev.com/api/"

    @Provides
    @Singleton
    fun provideClient(authTokenInterceptor: AuthTokenInterceptor, httpLoggingInterceptor: HttpLoggingInterceptor) =
        OkHttpClient.Builder().apply {
            followRedirects(false)
            followSslRedirects(false)
            connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            readTimeout(TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            addInterceptor(authTokenInterceptor)
            addInterceptor(httpLoggingInterceptor) //Всегда должен быть последним
        }.build()

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl("$HTTP_SCHEME$API_PATH")
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    fun provideAuthApi(retrofit: Retrofit): IAuthApi = retrofit.create(IAuthApi::class.java)

    @Provides
    fun provideAuthService(api: IAuthApi): IAuthService = AuthService(api)

    @Provides
    fun providePersonalInformationApi(retrofit: Retrofit): IPersonalInformationApi = retrofit.create(IPersonalInformationApi::class.java)

    @Provides
    @Singleton
    fun providePersonalInformationService(api: IPersonalInformationApi): IPersonalInformationService = PersonalInformationService(api)

    @Provides
    fun provideProcedureApi(retrofit: Retrofit): IProcedureApi = retrofit.create(IProcedureApi::class.java)

    @Provides
    fun provideProcedureService(api: IProcedureApi): IProcedureService = ProcedureService(api)

    @Provides
    fun providePhotoApi(retrofit: Retrofit): IPhotoApi = retrofit.create(IPhotoApi::class.java)

    @Provides
    fun providePhotoService(api: IPhotoApi): IPhotoService = PhotoService(api)

    @Provides
    fun provideSettingsApi(retrofit: Retrofit): ISettingsApi = retrofit.create(ISettingsApi::class.java)

    @Provides
    fun provideSettingsService(api: ISettingsApi): ISettingsService = SettingsService(api)
}
