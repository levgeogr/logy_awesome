package com.logy.android

import androidx.lifecycle.LiveData
import com.logy.android.utils.base.BaseViewModel
import com.logy.android.utils.provider.ConnectivityStatusProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AppActivityVM @Inject constructor(private val connectivityStatusProvider: ConnectivityStatusProvider) :
    BaseViewModel() {

    val connectivityLiveData: LiveData<ConnectivityStatusProvider.ConnectivityStatus>
        get() = connectivityStatusProvider.connectionStatusLiveData
}
