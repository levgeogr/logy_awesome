package com.logy.android.domain.models

import com.logy.android.R

enum class Country(val countryTag: String, val countryCode: String, val countryName: Int, val rawMask: String) {
    RUSSIA("russia", "+ 7", R.string.country_russia, "(___) ___-__-__"),
    UKRAINE("kazakhstan", "+ 380", R.string.country_kazakhstan, "(___) ___-___"),
    BELARUS("belarus", "+ 375", R.string.country_belarus, "(__) ___ __-__"),
    KAZAKHSTAN("ukraine", "+ 7", R.string.country_ukraine, "(___) ___-__-__"),
    USA("usa", "+ 1", R.string.country_usa, "(___) ___-____"),
    NO_COUNTRY("", "", 0, "");
}
