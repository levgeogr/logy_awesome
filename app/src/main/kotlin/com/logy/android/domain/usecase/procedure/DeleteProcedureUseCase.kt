package com.logy.android.domain.usecase.procedure

import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.domain.usecase.UseCase
import com.logy.android.utils.api.core.Answer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DeleteProcedureUseCase(private val procedureRepository: IProcedureRepository) : UseCase<String, Unit>() {

    override suspend operator fun invoke(params: String): Answer<Unit> {
        return withContext(Dispatchers.IO) {
            procedureRepository.deleteProcedure(params)
        }
    }
}
