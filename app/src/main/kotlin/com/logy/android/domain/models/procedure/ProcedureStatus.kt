package com.logy.android.domain.models.procedure

enum class ProcedureStatus(val value: String) {
    PLANNED("planned"),
    COMPLETE("complete"),
}
