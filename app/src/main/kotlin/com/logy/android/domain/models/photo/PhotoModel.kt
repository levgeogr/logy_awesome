package com.logy.android.domain.models.photo

data class PhotoModel(
    val id: String,
    val type: String,
    val imageFile: String
)
