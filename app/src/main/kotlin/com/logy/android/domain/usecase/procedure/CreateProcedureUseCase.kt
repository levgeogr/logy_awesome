package com.logy.android.domain.usecase.procedure

import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.domain.usecase.UseCase
import com.logy.android.domain.usecase.procedure.CreateProcedureUseCase.Params
import com.logy.android.utils.api.core.Answer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CreateProcedureUseCase(private val procedureRepository: IProcedureRepository) : UseCase<Params, Unit>() {

    override suspend operator fun invoke(params: Params): Answer<Unit> {
        return withContext(Dispatchers.IO) {
            procedureRepository.addProcedure(
                params.title,
                params.dateTime,
                params.comment,
                params.isNotificationNeeded,
                params.notificationTime,
                params.procedureStatus,
                params.listOfImagesBeforeIds,
                params.listOfImagesAfterIds,
            )
        }
    }

    class Params(
        val title: String,
        val dateTime: Long,
        val comment: String,
        val isNotificationNeeded: Boolean,
        val notificationTime: Long,
        val procedureStatus: ProcedureStatus,
        val listOfImagesBeforeIds: List<String>?,
        val listOfImagesAfterIds: List<String>?
    )
}
