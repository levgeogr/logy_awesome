package com.logy.android.domain.models.procedure

import android.os.Parcelable
import com.logy.android.data.remote.responses.procedure.ProcedureImage
import kotlinx.parcelize.Parcelize

@Parcelize
class ProcedureModel(
    val id: String,
    val type: String,
    val title: String,
    val plannedTime: Long,
    val comment: String,
    val status: ProcedureStatus,
    val isNotificated: Boolean,
    val images: List<ProcedureImage>,
    val firstImageBefore: ProcedureImage?,
    val firstImageAfter: ProcedureImage?,
) : Parcelable
