package com.logy.android.domain.models.profile

data class ReferralCodeModel(
    val referralCode: String,
    val referralCounter: Int
)
