package com.logy.android.domain.models.profile

import android.os.Parcelable
import com.logy.android.utils.ui.Gender
import kotlinx.parcelize.Parcelize

@Parcelize
data class PersonalInformationModel(
    val avatar: String?,
    val userName: String,
    val email: String,
    val birthday: String,
    val gender: Gender,
    val phoneNumber: Long,
) : Parcelable
