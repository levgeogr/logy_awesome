package com.logy.android.domain

import com.logy.android.domain.models.Country
import com.logy.android.domain.models.SettingsModel
import com.logy.android.domain.models.profile.PersonalInformationModel
import com.logy.android.domain.models.profile.ReferralCodeModel

interface IPrefSettings {
    var token: String?
    var user: PersonalInformationModel?
    var isOnboardNotPassed: Boolean
    var userSettings: SettingsModel
    var currentCountry: Country?
    var referralCode: ReferralCodeModel
    suspend fun clear()
}
