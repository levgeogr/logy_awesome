package com.logy.android.domain.models

class SettingsModel(
    var isReceivingNews: Boolean,
    var isReceivingNotifications: Boolean,
    var isIntegratedCalendar: Boolean
)
