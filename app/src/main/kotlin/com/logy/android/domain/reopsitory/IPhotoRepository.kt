package com.logy.android.domain.reopsitory

import com.logy.android.domain.models.photo.PhotoModel
import com.logy.android.utils.api.core.Answer
import com.logy.android.utils.helpers.GetImageFromGalleryHelper
import java.io.File

interface IPhotoRepository {
    suspend fun sendPhoto(photo: File, type: GetImageFromGalleryHelper.PhotoTypes): Answer<PhotoModel>
}
