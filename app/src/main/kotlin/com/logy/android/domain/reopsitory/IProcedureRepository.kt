package com.logy.android.domain.reopsitory

import androidx.paging.PagingData
import com.logy.android.domain.models.procedure.ProcedureModel
import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.utils.api.core.Answer
import kotlinx.coroutines.flow.Flow

interface IProcedureRepository {

    fun getProcedures(): Flow<List<ProcedureModel>>

    suspend fun syncProcedures(): Answer<List<ProcedureModel>>

    suspend fun addProcedure(
        title: String,
        dateTime: Long,
        comment: String,
        isNotificated: Boolean,
        notificationTime: Long,
        procedureStatus: ProcedureStatus,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<Unit>

    suspend fun updateProcedure(
        id: String,
        title: String,
        dateTime: Long,
        comment: String,
        isNotificated: Boolean,
        procedureStatus: ProcedureStatus,
        listOfImagesBeforeIds: List<String>?,
        listOfImagesAfterIds: List<String>?
    ): Answer<Unit>

    suspend fun deleteProcedure(id: String): Answer<Unit>

    fun getPagingProcedures(): Flow<PagingData<ProcedureModel>>
}
