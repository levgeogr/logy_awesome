package com.logy.android.domain.usecase.photo

import com.logy.android.domain.models.photo.PhotoModel
import com.logy.android.domain.reopsitory.IPhotoRepository
import com.logy.android.domain.usecase.UseCase
import com.logy.android.utils.api.core.Answer
import com.logy.android.utils.helpers.GetImageFromGalleryHelper
import java.io.File
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LoadPhotoUseCase(private val repository: IPhotoRepository) :
    UseCase<LoadPhotoUseCase.Params, PhotoModel>() {

    override suspend fun invoke(params: Params): Answer<PhotoModel> {
        return withContext(Dispatchers.IO) {
            repository.sendPhoto(params.avatar, params.type)
        }
    }

    class Params(
        val avatar: File,
        val type: GetImageFromGalleryHelper.PhotoTypes
    )
}
