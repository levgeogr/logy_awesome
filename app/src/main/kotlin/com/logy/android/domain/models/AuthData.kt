package com.logy.android.domain.models

data class AuthData(
    val token: String
)
