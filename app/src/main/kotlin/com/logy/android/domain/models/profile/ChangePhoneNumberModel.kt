package com.logy.android.domain.models.profile

data class ChangePhoneNumberModel(
    val time: Int,
    val newPhoneNumber: Long
)
