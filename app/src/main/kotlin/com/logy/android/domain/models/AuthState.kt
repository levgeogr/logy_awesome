package com.logy.android.domain.models

sealed class AuthState {
    object LoggedIn : AuthState()
    object LoggedOut : AuthState()
    object ShowOnboard : AuthState()
}
