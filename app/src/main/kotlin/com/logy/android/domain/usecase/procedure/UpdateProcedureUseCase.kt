package com.logy.android.domain.usecase.procedure

import com.logy.android.domain.models.procedure.ProcedureStatus
import com.logy.android.domain.reopsitory.IProcedureRepository
import com.logy.android.domain.usecase.UseCase
import com.logy.android.domain.usecase.procedure.UpdateProcedureUseCase.Params
import com.logy.android.utils.api.core.Answer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UpdateProcedureUseCase(private val procedureRepository: IProcedureRepository) : UseCase<Params, Unit>() {

    override suspend operator fun invoke(params: Params): Answer<Unit> {
        return withContext(Dispatchers.IO) {
            procedureRepository.updateProcedure(
                id = params.id,
                title = params.title,
                dateTime = params.dateTime,
                comment = params.comment,
                isNotificated = params.isNotificated,
                procedureStatus = params.procedureStatus,
                listOfImagesBeforeIds = params.listOfImagesBeforeIds,
                listOfImagesAfterIds = params.listOfImagesAfterIds
            )
        }
    }

    class Params(
        val id: String,
        val title: String,
        val dateTime: Long,
        val comment: String,
        val isNotificated: Boolean,
        val procedureStatus: ProcedureStatus,
        val listOfImagesBeforeIds: List<String>?,
        val listOfImagesAfterIds: List<String>?
    )
}
