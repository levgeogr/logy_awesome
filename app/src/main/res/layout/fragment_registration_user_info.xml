<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data class="FragmentRegistrationUserInfoBinding">

        <variable
            name="vm"
            type="com.logy.android.presentation.auth_screens.registration.user_info.RegistrationUserInfoVM" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <com.logy.android.utils.custom_view.StretchingImageView
            android:id="@+id/stretchingImageView"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:src="@drawable/bg_main" />

        <View
            android:id="@+id/switchLanguageButton"
            android:layout_width="80dp"
            android:layout_height="60dp"
            android:background="@drawable/ic_switch_language"
            android:clickable="true"
            android:elevation="15dp"
            android:focusable="true"
            android:onClick="@{() -> vm.showLanguageChangeDialog()}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <ScrollView
            android:id="@+id/scrollView"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:fillViewport="true"
            android:overScrollMode="never"
            android:scrollbarThumbVertical="@android:color/transparent">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/parentConstraintLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:paddingHorizontal="@dimen/screensHorizontalPadding"
                android:paddingTop="?attr/actionBarSize"
                tools:context=".android.presentation.auth_screens.registration.user_info.RegistrationUserInfoFragment"
                tools:ignore="ContentDescription,SpUsage">

                <TextView
                    android:id="@+id/screenTitleTextView"
                    style="@style/TitleScreen"
                    android:layout_marginTop="16dp"
                    android:text="@string/registration_user_info_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <TextView
                    android:id="@+id/fillInfoTextView"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="20dp"
                    android:text="@string/registration_user_info_fill_info"
                    android:textAlignment="center"
                    android:textColor="@color/main_black"
                    app:layout_constraintTop_toBottomOf="@id/screenTitleTextView" />

                <de.hdodenhof.circleimageview.CircleImageView
                    android:id="@+id/avatarImageView"
                    android:layout_width="144dp"
                    android:layout_height="144dp"
                    android:layout_marginTop="20dp"
                    android:onClick="@{() -> vm.loadImageFromGallery.invoke()}"
                    android:src="@color/light_blue"
                    app:layout_constraintEnd_toEndOf="@id/fillInfoTextView"
                    app:layout_constraintStart_toStartOf="@id/fillInfoTextView"
                    app:layout_constraintTop_toBottomOf="@id/fillInfoTextView"
                    app:stringUrl="@{vm.avatar}" />

                <ProgressBar
                    android:id="@+id/avatarProgressBar"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:indeterminateTint="@color/pink"
                    app:layout_constraintEnd_toEndOf="@+id/avatarImageView"
                    app:layout_constraintStart_toStartOf="@+id/avatarImageView"
                    app:layout_constraintTop_toTopOf="@+id/avatarImageView"
                    app:layout_constraintBottom_toBottomOf="@+id/avatarImageView"
                    app:visibility="@{vm.avatarVisibilityStatus}" />

                <ImageView
                    android:id="@+id/photoIconImageView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:layout_constraintBottom_toTopOf="@id/photoIconTextView"
                    app:layout_constraintEnd_toEndOf="@id/avatarImageView"
                    app:layout_constraintStart_toStartOf="@id/avatarImageView"
                    app:layout_constraintTop_toTopOf="@id/avatarImageView"
                    app:layout_constraintVertical_chainStyle="packed"
                    app:srcCompat="@drawable/ic_camera"
                    app:tint="@color/white"
                    app:visibility="@{vm.avatar == null}" />

                <TextView
                    android:id="@+id/photoIconTextView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="8dp"
                    android:fontFamily="@font/roboto_regular"
                    android:text="@string/registration_user_info_hint_profile_picture"
                    android:textAlignment="center"
                    android:textColor="@color/white"
                    android:textSize="13dp"
                    app:layout_constraintBottom_toBottomOf="@id/avatarImageView"
                    app:layout_constraintEnd_toEndOf="@id/avatarImageView"
                    app:layout_constraintStart_toStartOf="@id/avatarImageView"
                    app:layout_constraintTop_toBottomOf="@id/photoIconImageView"
                    app:visibility="@{vm.avatar == null}" />

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/nameInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginTop="20dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/avatarImageView">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/nameInputField"
                        style="@style/DefaultTextInputEditText"
                        android:digits="ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghjiklmnopqrstuvwxyzАБВГДЙЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабйвгдеёжзиклмнопрстуфхцчшщьыъэюя"
                        android:inputType="textPersonName"
                        android:text="@={vm.name}"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray"
                        app:bindErrorWrapperText="@{vm.nameErrorWrapper}"
                        app:hasMediumWeightInput="@{true}"
                        app:unfocusedHint="@{@string/registration_user_info_hint_name}"
                        tools:hint="@string/registration_user_info_hint_name" />
                </com.google.android.material.textfield.TextInputLayout>

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/passwordInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginTop="4dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/nameInputLayout">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/passwordInputField"
                        style="@style/DefaultTextInputEditText"
                        android:inputType="textVisiblePassword"
                        android:text="@={vm.password}"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray"
                        app:bindErrorWrapperText="@{vm.passErrorWrapper}"
                        app:hasMediumWeightInput="@{true}"
                        app:unfocusedHint="@{@string/registration_user_info_hint_password}"
                        tools:hint="@string/registration_user_info_hint_password" />
                </com.google.android.material.textfield.TextInputLayout>

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/repeatPasswordInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginTop="4dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/passwordInputLayout">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/repeatPasswordInputField"
                        style="@style/DefaultTextInputEditText"
                        android:inputType="textVisiblePassword"
                        android:text="@={vm.repeatedPassword}"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray"
                        app:bindErrorWrapperText="@{vm.repeatedPassErrorWrapper}"
                        app:hasMediumWeightInput="@{true}"
                        app:unfocusedHint="@{@string/registration_user_info_hint_repeat_password}"
                        tools:hint="@string/registration_user_info_hint_repeat_password" />
                </com.google.android.material.textfield.TextInputLayout>

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/emailInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginTop="4dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/repeatPasswordInputLayout">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/emailInputField"
                        style="@style/DefaultTextInputEditText"
                        android:inputType="textEmailAddress"
                        android:text="@={vm.email}"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray"
                        app:bindErrorWrapperText="@{vm.emailErrorWrapper}"
                        app:hasMediumWeightInput="@{true}"
                        app:unfocusedHint="@{@string/registration_user_info_hint_email}"
                        tools:hint="@string/registration_user_info_hint_email" />
                </com.google.android.material.textfield.TextInputLayout>

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/dateOfBirthInputLayout"
                    style="@style/BirthdayLayout"
                    android:layout_marginTop="4dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/emailInputLayout">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/dateOfBirthInputField"
                        style="@style/BirthdayField"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray"
                        app:bindErrorWrapperText="@{vm.dateErrorWrapper}"
                        app:date="@{vm.dateOfBirth}"
                        app:hasMediumWeightInput="@{true}"
                        app:unfocusedHint="@{@string/registration_user_info_hint_date_of_birth}"
                        tools:hint="@string/registration_user_info_hint_date_of_birth" />
                </com.google.android.material.textfield.TextInputLayout>

                <Spinner
                    android:id="@+id/sexSpinner"
                    android:layout_width="0dp"
                    android:layout_height="57dp"
                    android:layout_gravity="center_vertical"
                    android:layout_marginTop="4dp"
                    android:background="@drawable/bg_edit_text"
                    android:drawableEnd="@drawable/ic_arrow_right"
                    android:entries="@array/sex_array"
                    android:includeFontPadding="true"
                    android:spinnerMode="dropdown"
                    android:textSize="15dp"
                    app:bindErrorWrapperText="@{vm.genderErrorWrapper}"
                    app:gender="@={vm.gender}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/dateOfBirthInputLayout" />

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/referralCodeInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginTop="4dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/sexSpinner">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/referralCodeInputField"
                        style="@style/DefaultTextInputEditText"
                        android:fontFamily="@font/roboto_regular"
                        android:inputType="text"
                        android:text="@={vm.referralCode}"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray"
                        app:hasMediumWeightInput="@{true}"
                        app:unfocusedHint="@{@string/registration_user_info_hint_referral_code}"
                        tools:hint="@string/registration_user_info_hint_referral_code" />
                </com.google.android.material.textfield.TextInputLayout>

                <TextView
                    android:id="@+id/errorTextView"
                    style="@style/ErrorTextView"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:bindErrorWrapperText="@{vm.currentErrorWrapper}"
                    app:errorDisplayText="@{true}"
                    app:layout_constraintBottom_toTopOf="@id/registerButton"
                    app:layout_constraintTop_toBottomOf="@id/referralCodeInputLayout"
                    app:layout_constraintVertical_bias="1"
                    tools:text="Ошибка" />

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/registerButton"
                    style="@style/RoundButton"
                    android:layout_width="match_parent"
                    android:layout_height="48dp"
                    android:layout_marginTop="42dp"
                    android:layout_marginBottom="40dp"
                    android:enabled="@{Boolean.logicalAnd(!vm.loaderVisibilityStatus, !vm.avatarVisibilityStatus)}"
                    android:onClick="@{() -> vm.completeRegistration()}"
                    android:text="@string/registration_user_info_finish_registration"
                    android:textSize="19dp"
                    app:backgroundTint="@drawable/bg_tint_pressed_pink"
                    app:layout_constraintBottom_toBottomOf="parent" />

            </androidx.constraintlayout.widget.ConstraintLayout>
        </ScrollView>

        <ProgressBar
            android:id="@+id/progressBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:indeterminateTint="@color/pink"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            app:visibility="@{vm.loaderVisibilityStatus}" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
