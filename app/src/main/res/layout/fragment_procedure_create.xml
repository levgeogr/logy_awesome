<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="com.logy.android.domain.models.procedure.ProcedureStatus" />

        <variable
            name="vm"
            type="com.logy.android.presentation.procedures.create.ProcedureCreateVM" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@drawable/bg_main_no_text"
        tools:ignore="SpUsage, ContentDescription, RtlSymmetry">

        <include
            android:id="@+id/toolbar"
            layout="@layout/toolbar"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <ScrollView
            android:id="@+id/procedureCreateScrollView"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:overScrollMode="never"
            android:scrollbars="none"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/toolbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <TextView
                    android:id="@+id/createProcedureTitleTextView"
                    style="@style/TitleScreen"
                    android:layout_marginTop="20dp"
                    android:text="@string/procedures_records_create_procedure"
                    android:textColor="@color/main_black"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    app:visibility="@{!vm.isEditProcedure()}" />

                <TextView
                    android:id="@+id/createProcedureDescriptionTextView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="20dp"
                    android:fontFamily="@font/roboto_regular"
                    android:lineSpacingExtra="4sp"
                    android:paddingHorizontal="36dp"
                    android:text="@string/create_procedure_description"
                    android:textAlignment="center"
                    android:textColor="@color/main_black"
                    android:textSize="15dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/createProcedureTitleTextView"
                    app:visibility="@{!vm.isEditProcedure()}" />

                <RadioGroup
                    android:id="@+id/procedureTypeRadioGroup"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="20dp"
                    android:orientation="horizontal"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/createProcedureDescriptionTextView"
                    app:visibility="@{!vm.isEditProcedure()}">

                    <RadioButton
                        android:id="@+id/plannedRadioButton"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:background="@drawable/custom_btn_radio_restore_password_left"
                        android:button="@null"
                        android:checked="@{vm.procedureStatus.equals(ProcedureStatus.PLANNED)}"
                        android:fontFamily="@font/roboto_bold"
                        android:onClick="@{() -> vm.setProcedureStatus(ProcedureStatus.PLANNED)}"
                        android:text="@string/create_procedure_planned"
                        android:textAlignment="center"
                        android:textColor="@drawable/custom_text_color_selector"
                        android:textSize="15dp"
                        android:textStyle="bold"
                        app:backgroundTint="@null"
                        tools:backgroundTint="@color/pink" />

                    <RadioButton
                        android:id="@+id/executedRadioButton"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:background="@drawable/custom_btn_radio_restore_password_right"
                        android:button="@null"
                        android:checked="@{vm.procedureStatus.equals(ProcedureStatus.COMPLETE)}"
                        android:fontFamily="@font/roboto_bold"
                        android:onClick="@{() -> vm.setProcedureStatus(ProcedureStatus.COMPLETE)}"
                        android:text="@string/create_procedure_executed"
                        android:textAlignment="center"
                        android:textColor="@drawable/custom_text_color_selector"
                        android:textSize="15dp"
                        android:textStyle="bold"
                        app:backgroundTint="@null" />
                </RadioGroup>

                <TextView
                    android:id="@+id/nameProcedureTitleTextView"
                    style="@style/TitleScreen"
                    android:layout_marginStart="18dp"
                    android:layout_marginTop="22dp"
                    android:text="@string/procedure_info_procedure_name"
                    android:textColor="@color/grey"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/procedureTypeRadioGroup" />

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/procedureNameTextInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="22dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/nameProcedureTitleTextView">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/procedureNameTextInputEditText"
                        style="@style/DefaultTextInputEditText"
                        android:hint="@string/procedure_info_name"
                        android:text="@={vm.procedureName}"
                        android:textColorHint="@color/light_grey" />
                </com.google.android.material.textfield.TextInputLayout>

                <TextView
                    android:id="@+id/dateAndTimeTextView"
                    style="@style/TitleScreen"
                    android:layout_marginStart="18dp"
                    android:layout_marginTop="26dp"
                    android:text="@string/procedure_info_date_and_time"
                    android:textColor="@color/grey"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/procedureNameTextInputLayout" />

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/dateTextInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="20dp"
                    android:clickable="true"
                    android:focusable="true"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/dateAndTimeTextView">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/dateTextInputField"
                        style="@style/DefaultTextInputEditText"
                        android:clickable="false"
                        android:cursorVisible="false"
                        android:focusable="false"
                        android:focusableInTouchMode="false"
                        android:hint="@string/create_procedure_select_date"
                        android:text="@={vm.date}"
                        android:textColor="@color/main_black"
                        android:textColorHint="@color/normal_gray" />
                </com.google.android.material.textfield.TextInputLayout>

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/timeTextInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="12dp"
                    android:clickable="true"
                    android:focusable="true"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/dateTextInputLayout">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/timeTextInputEditText"
                        style="@style/DefaultTextInputEditText"
                        android:clickable="false"
                        android:cursorVisible="false"
                        android:focusable="false"
                        android:hint="@string/create_procedure_time"
                        android:inputType="time"
                        android:text="@={vm.time}"
                        android:textColorHint="@color/light_grey" />
                </com.google.android.material.textfield.TextInputLayout>

                <TextView
                    android:id="@+id/notificationTextView"
                    style="@style/TitleScreen"
                    android:layout_marginStart="18dp"
                    android:layout_marginTop="26dp"
                    android:text="@string/procedure_info_notification"
                    android:textColor="@color/grey"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/timeTextInputLayout" />

                <TextView
                    android:id="@+id/createProcedureNotificationTextView"
                    style="@style/DefaultTextInputLayout"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="20dp"
                    android:background="@drawable/bg_edit_text"
                    android:gravity="center_vertical"
                    android:lineSpacingExtra="4dp"
                    android:paddingStart="16dp"
                    android:paddingEnd="32dp"
                    android:text="@string/procedure_info_not_installed"
                    android:textColor="@{vm.notificationEnabled ? @color/main_black : @color/light_grey}"
                    android:textSize="15dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/notificationTextView"
                    tools:text="@string/procedure_info_not_installed" />

                <ImageView
                    android:id="@+id/notificationBellImageView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:onClick="@{() -> vm.toggleNotificationEnabled()}"
                    android:padding="16dp"
                    android:src="@{vm.notificationEnabled ? @drawable/ic_pink_bell: @drawable/ic_alert_crossed_out}"
                    app:layout_constraintBottom_toBottomOf="@+id/createProcedureNotificationTextView"
                    app:layout_constraintEnd_toEndOf="@+id/createProcedureNotificationTextView"
                    app:layout_constraintTop_toTopOf="@+id/createProcedureNotificationTextView"
                    tools:src="@drawable/ic_alert_crossed_out" />

                <androidx.constraintlayout.widget.Group
                    android:id="@+id/notificationGroup"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:constraint_referenced_ids="notificationBellImageView,createProcedureNotificationTextView,notificationTextView" />

                <TextView
                    android:id="@+id/commentTextView"
                    style="@style/TitleScreen"
                    android:layout_marginStart="18dp"
                    android:layout_marginTop="26dp"
                    android:text="@string/procedure_info_comment"
                    android:textColor="@color/grey"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/createProcedureNotificationTextView" />

                <com.google.android.material.textfield.TextInputLayout
                    android:id="@+id/commentTextInputLayout"
                    style="@style/DefaultTextInputLayout"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="20dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/commentTextView">

                    <com.google.android.material.textfield.TextInputEditText
                        android:id="@+id/commentTextInputEditText"
                        style="@style/DefaultTextInputEditText"
                        android:layout_height="wrap_content"
                        android:background="@drawable/bg_rounded_textview_11dp"
                        android:inputType="textMultiLine"
                        android:maxLength="1000"
                        android:maxLines="5"
                        android:minLines="2"
                        android:overScrollMode="always"
                        android:padding="11dp"
                        android:scrollbarStyle="insideInset"
                        android:scrollbars="vertical"
                        android:text="@={vm.commentMedicationText}"
                        android:textColorHint="@color/light_grey"
                        app:hint="@{@string/create_procedure_comment}"
                        tools:hint="@string/create_procedure_comment" />
                </com.google.android.material.textfield.TextInputLayout>

                <TextView
                    android:id="@+id/createPictureTextView"
                    style="@style/TitleScreen"
                    android:layout_marginStart="18dp"
                    android:layout_marginTop="26dp"
                    android:text="@{vm.procedureStatus.value == ProcedureStatus.PLANNED.value ? @string/create_procedure_upload_image_before : @string/create_procedure_upload_image_after}"
                    android:textColor="@color/grey"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/commentTextInputLayout" />

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/procedureInfoWasCompletedBtn"
                    style="@style/RoundButton"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="58dp"
                    android:onClick="@{() -> vm.changeProcedureStatus()}"
                    android:text="@string/procedure_info_procedure_was_completed"
                    app:backgroundTint="@drawable/bg_tint_pressed_main_black"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/cameraImageView"
                    app:visibility="@{Boolean.logicalAnd(vm.isEditProcedure(), vm.procedureStatus.value == ProcedureStatus.PLANNED.value)}" />

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/saveButton"
                    style="@style/RoundButton"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="18dp"
                    android:enabled="@{vm.saveEnabled}"
                    android:text="@string/procedure_info_save"
                    android:textStyle="bold"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/procedureInfoWasCompletedBtn" />

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/cancelButton"
                    style="@style/Widget.MaterialComponents.Button.TextButton"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:onClick="@{() -> vm.cancelProcedure()}"
                    android:layout_marginHorizontal="18dp"
                    android:layout_marginTop="18dp"
                    android:layout_marginBottom="94dp"
                    android:fontFamily="@font/times_new_roman"
                    android:insetTop="0dp"
                    android:insetBottom="0dp"
                    android:letterSpacing="0.03"
                    android:text="@string/personal_cancel"
                    android:textAlignment="center"
                    android:textAllCaps="false"
                    android:textColor="@color/main_black"
                    android:textSize="19dp"
                    android:textStyle="bold"
                    app:cornerRadius="46dp"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/saveButton"
                    app:strokeWidth="1dp" />

                <androidx.legacy.widget.Space
                    android:layout_width="wrap_content"
                    android:layout_height="100dp"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/cancelButton" />

                <ImageView
                    android:id="@+id/cameraImageView"
                    android:layout_width="104dp"
                    android:layout_height="104dp"
                    android:layout_marginStart="18dp"
                    android:layout_marginTop="20dp"
                    android:background="@drawable/bg_rounded_textview_transparent_with_stroke_18dp"
                    android:onClick="@{() -> vm.onImageClick()}"
                    android:padding="34dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/createPictureTextView"
                    app:srcCompat="@drawable/ic_camera" />

                <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/createProcedureRecyclerView"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="20dp"
                    android:clipToPadding="false"
                    android:layout_marginStart="6dp"
                    android:orientation="horizontal"
                    android:overScrollMode="never"
                    android:paddingEnd="18dp"
                    app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@+id/cameraImageView"
                    app:layout_constraintTop_toBottomOf="@+id/createPictureTextView"
                    app:setOnlyAdapter="@{vm.adapter}"
                    tools:listitem="@layout/item_procedure_info_photo" />

            </androidx.constraintlayout.widget.ConstraintLayout>
        </ScrollView>
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
